Paclet[
	Name -> "Objects",
	Version -> "1.31",
	MathematicaVersion -> "10+",
	Description -> "Mathematica module for writing object-oriented code.",
    Creator -> "Kirill T. Mingulov",
	Extensions -> {
		{
			"Kernel",
			"Context" -> {"Objects`"}
		},
		{
			"Documentation",
			Language -> "English",
			LinkBase -> "Objects"
		}
	}
]

--------------------------------------------------------------------------------
v1.31                                                                 02.11.2017
--------------------------------------------------------------------------------

* Added Python-like syntax for methods. Arguments, which have default values,
  can be supplied by their names in any order.
* Added several checks in OSave and OLoad. Now OSave can save not only objects,
  but also auxiliary symbols.

--------------------------------------------------------------------------------
v1.3                                                                  20.10.2017
--------------------------------------------------------------------------------

* Changed architecture and syntax of classes. Added keywords $Destructor and 
  $Method. Changed meaning of $Constructor.
* Improved error messages and warnings.

--------------------------------------------------------------------------------
v1.22                                                                 19.09.2017
--------------------------------------------------------------------------------

* Fixed minor bugs.
* Added ObjectsTrace, which prints methods' call stack.

--------------------------------------------------------------------------------
v1.21                                                                 04.09.2017
--------------------------------------------------------------------------------

* Fixed minor bugs, introduced in previous version.

--------------------------------------------------------------------------------
v1.2                                                                  01.09.2017
--------------------------------------------------------------------------------

* Added OClone, which completely clones an existing object.
* Added ObjectsProfile, which measures time spent in objects' methods.
* Added $Constructor keyword, which allows to call one constructor within 
  another.
* Added logo.

--------------------------------------------------------------------------------
v1.1                                                                  26.05.2017
--------------------------------------------------------------------------------

* Now CNew can create classes with several constructors.
* Now ONew requires a sequences of constructor arguments, not a list.
* Added $ObjectsVersion.

--------------------------------------------------------------------------------
v1.0                                                                  19.05.2017
--------------------------------------------------------------------------------

Initial version.

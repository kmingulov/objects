# Checking whether the version is given.
if [[ $1 = "" ]]; then
    echo "Usage: $0 version"
    exit
fi

# Working directory.
workdir=`pwd`

# Echoing the destination folder.
echo "Package version:    $1"
echo ""

# Creating directories.
mkdir "/tmp/Objects/"

# Copying package files.
echo -n "Copying package files... "
cd ../
cp -r Objects.m README.md Documentation/ DistributionFiles/* "/tmp/Objects"
echo "Done."

# Creating the archive.
echo -n "Creating the archive... "
cd "/tmp"
zip -r "$workdir/../$1.zip" "Objects/"
echo "Done."

# Removing files.
rm -rf "/tmp/Objects/"

# Everything's done.
echo ""
echo "Everything is done."

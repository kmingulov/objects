(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 10.4' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     14619,        345]
NotebookOptionsPosition[     14193,        327]
NotebookOutlinePosition[     14528,        342]
CellTagsIndexPosition[     14485,        339]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData["Quit"], "Input",
 CellChangeTimes->{{3.703916074619433*^9, 3.703916075611413*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"SetDirectory", "[", 
   RowBox[{"NotebookDirectory", "[", "]"}], "]"}], ";"}]], "Input",
 CellChangeTimes->{{3.703909532497007*^9, 3.703909537895031*^9}}],

Cell[BoxData[{
 RowBox[{
  RowBox[{"symbols", "=", 
   RowBox[{"AbsoluteFileName", "/@", 
    RowBox[{
    "FileNames", "[", 
     "\"\<../Documentation/English/ReferencePages/Symbols/*\>\"", "]"}]}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"guides", "=", 
   RowBox[{"AbsoluteFileName", "/@", 
    RowBox[{
    "FileNames", "[", "\"\<../Documentation/English/Guides/*\>\"", "]"}]}]}], 
  ";"}]}], "Input",
 CellChangeTimes->{{3.70390953990736*^9, 3.703909589815509*^9}, {
   3.703909975492876*^9, 3.7039099760053596`*^9}, 3.704083427033313*^9, {
   3.704162999692976*^9, 3.704163013375535*^9}, {3.704163073865541*^9, 
   3.704163086603167*^9}}],

Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{"LockSymbol", "[", "file_", "]"}], ":=", 
   RowBox[{"Module", "[", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"sym", ",", "nb"}], "}"}], ",", "\[IndentingNewLine]", 
     RowBox[{
      RowBox[{"sym", "=", 
       RowBox[{"StringDrop", "[", 
        RowBox[{
         RowBox[{"FileNameTake", "[", "file", "]"}], ",", 
         RowBox[{"-", "3"}]}], "]"}]}], ";", "\[IndentingNewLine]", 
      RowBox[{"nb", "=", 
       RowBox[{"NotebookOpen", "[", "file", "]"}]}], ";", 
      "\[IndentingNewLine]", 
      RowBox[{"SetOptions", "[", 
       RowBox[{"nb", ",", 
        RowBox[{"TaggingRules", "\[Rule]", 
         RowBox[{"{", 
          RowBox[{
           RowBox[{"\"\<ModificationHighlight\>\"", "\[Rule]", "False"}], ",", 
           RowBox[{"\"\<Metadata\>\"", "\[Rule]", 
            RowBox[{"{", 
             RowBox[{
              RowBox[{"\"\<context\>\"", "\[Rule]", "\"\<Objects`\>\""}], ",", 
              RowBox[{"\"\<keywords\>\"", "\[Rule]", 
               RowBox[{"{", "}"}]}], ",", 
              RowBox[{"\"\<index\>\"", "\[Rule]", "True"}], ",", 
              RowBox[{
              "\"\<label\>\"", "\[Rule]", "\"\<Objects Package Symbol\>\""}], 
              ",", 
              RowBox[{"\"\<language\>\"", "\[Rule]", "\"\<en\>\""}], ",", 
              RowBox[{
              "\"\<paclet\>\"", "\[Rule]", "\"\<Objects Package\>\""}], ",", 
              RowBox[{"\"\<status\>\"", "\[Rule]", "\"\<\>\""}], ",", 
              RowBox[{"\"\<summary\>\"", "\[Rule]", "\"\<\>\""}], ",", 
              RowBox[{"\"\<synonyms\>\"", "\[Rule]", 
               RowBox[{"{", "}"}]}], ",", 
              RowBox[{"\"\<title\>\"", "\[Rule]", "sym"}], ",", 
              RowBox[{"\"\<type\>\"", "\[Rule]", "\"\<Symbol\>\""}], ",", 
              RowBox[{"\"\<uri\>\"", "\[Rule]", 
               RowBox[{"\"\<Objects/ref/\>\"", "<>", "sym"}]}]}], "}"}]}], 
           ",", 
           RowBox[{"\"\<SearchTextTranslated\>\"", "\[Rule]", "\"\<\>\""}]}], 
          "}"}]}]}], "]"}], ";", "\[IndentingNewLine]", 
      RowBox[{"SetOptions", "[", 
       RowBox[{"nb", ",", 
        RowBox[{"Saveable", "\[Rule]", "False"}]}], "]"}], ";", 
      "\[IndentingNewLine]", 
      RowBox[{"SetOptions", "[", 
       RowBox[{"nb", ",", 
        RowBox[{"StyleDefinitions", "\[Rule]", 
         RowBox[{"FrontEnd`FileName", "[", 
          RowBox[{
           RowBox[{"{", "\"\<Wolfram\>\"", "}"}], ",", 
           "\"\<Reference.nb\>\""}], "]"}]}]}], "]"}], ";", 
      "\[IndentingNewLine]", 
      RowBox[{"NotebookSave", "[", "nb", "]"}], ";", "\[IndentingNewLine]", 
      RowBox[{"NotebookClose", "[", "nb", "]"}], ";"}]}], 
    "\[IndentingNewLine]", "]"}]}], ";"}], "\n", 
 RowBox[{
  RowBox[{
   RowBox[{"LockGuide", "[", "file_", "]"}], ":=", 
   RowBox[{"Module", "[", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"sym", ",", "nb"}], "}"}], ",", "\[IndentingNewLine]", 
     RowBox[{
      RowBox[{"sym", "=", 
       RowBox[{"StringDrop", "[", 
        RowBox[{
         RowBox[{"FileNameTake", "[", "file", "]"}], ",", 
         RowBox[{"-", "3"}]}], "]"}]}], ";", "\[IndentingNewLine]", 
      RowBox[{"nb", "=", 
       RowBox[{"NotebookOpen", "[", "file", "]"}]}], ";", 
      "\[IndentingNewLine]", 
      RowBox[{"SetOptions", "[", 
       RowBox[{"nb", ",", 
        RowBox[{"TaggingRules", "\[Rule]", 
         RowBox[{"{", 
          RowBox[{
           RowBox[{"\"\<ModificationHighlight\>\"", "\[Rule]", "False"}], ",", 
           RowBox[{"\"\<Metadata\>\"", "\[Rule]", 
            RowBox[{"{", 
             RowBox[{
              RowBox[{"\"\<context\>\"", "\[Rule]", "\"\<Objects`\>\""}], ",", 
              RowBox[{"\"\<keywords\>\"", "\[Rule]", 
               RowBox[{"{", "}"}]}], ",", 
              RowBox[{"\"\<index\>\"", "\[Rule]", "True"}], ",", 
              RowBox[{
              "\"\<label\>\"", "\[Rule]", "\"\<Objects Package Guide\>\""}], 
              ",", 
              RowBox[{"\"\<language\>\"", "\[Rule]", "\"\<en\>\""}], ",", 
              RowBox[{
              "\"\<paclet\>\"", "\[Rule]", "\"\<Objects Package\>\""}], ",", 
              RowBox[{"\"\<status\>\"", "\[Rule]", "\"\<\>\""}], ",", 
              RowBox[{"\"\<summary\>\"", "\[Rule]", "\"\<\>\""}], ",", 
              RowBox[{"\"\<synonyms\>\"", "\[Rule]", 
               RowBox[{"{", "}"}]}], ",", 
              RowBox[{"\"\<title\>\"", "\[Rule]", "sym"}], ",", 
              RowBox[{"\"\<type\>\"", "\[Rule]", "\"\<Symbol\>\""}], ",", 
              RowBox[{"\"\<uri\>\"", "\[Rule]", 
               RowBox[{"\"\<Objects/guide/\>\"", "<>", "sym"}]}]}], "}"}]}], 
           ",", 
           RowBox[{"\"\<SearchTextTranslated\>\"", "\[Rule]", "\"\<\>\""}]}], 
          "}"}]}]}], "]"}], ";", "\[IndentingNewLine]", 
      RowBox[{"SetOptions", "[", 
       RowBox[{"nb", ",", 
        RowBox[{"Saveable", "\[Rule]", "False"}]}], "]"}], ";", 
      "\[IndentingNewLine]", 
      RowBox[{"SetOptions", "[", 
       RowBox[{"nb", ",", 
        RowBox[{"StyleDefinitions", "\[Rule]", 
         RowBox[{"FrontEnd`FileName", "[", 
          RowBox[{
           RowBox[{"{", "\"\<Wolfram\>\"", "}"}], ",", 
           "\"\<Reference.nb\>\""}], "]"}]}]}], "]"}], ";", 
      "\[IndentingNewLine]", 
      RowBox[{"NotebookSave", "[", "nb", "]"}], ";", "\[IndentingNewLine]", 
      RowBox[{"NotebookClose", "[", "nb", "]"}], ";"}]}], 
    "\[IndentingNewLine]", "]"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"UnlockSymbol", "[", "file_", "]"}], ":=", 
   RowBox[{"Module", "[", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"sym", ",", "nb"}], "}"}], ",", "\[IndentingNewLine]", 
     RowBox[{
      RowBox[{"sym", "=", 
       RowBox[{"StringDrop", "[", 
        RowBox[{
         RowBox[{"FileNameTake", "[", "file", "]"}], ",", 
         RowBox[{"-", "3"}]}], "]"}]}], ";", "\[IndentingNewLine]", 
      RowBox[{"nb", "=", 
       RowBox[{"NotebookOpen", "[", "file", "]"}]}], ";", 
      "\[IndentingNewLine]", 
      RowBox[{"SetOptions", "[", 
       RowBox[{"nb", ",", 
        RowBox[{"TaggingRules", "\[Rule]", 
         RowBox[{"{", 
          RowBox[{
           RowBox[{"\"\<ModificationHighlight\>\"", "\[Rule]", "False"}], ",", 
           RowBox[{"\"\<Metadata\>\"", "\[Rule]", 
            RowBox[{"{", 
             RowBox[{
              RowBox[{"\"\<context\>\"", "\[Rule]", "\"\<Objects`\>\""}], ",", 
              RowBox[{"\"\<keywords\>\"", "\[Rule]", 
               RowBox[{"{", "}"}]}], ",", 
              RowBox[{"\"\<index\>\"", "\[Rule]", "True"}], ",", 
              RowBox[{
              "\"\<label\>\"", "\[Rule]", "\"\<Objects Package Symbol\>\""}], 
              ",", 
              RowBox[{"\"\<language\>\"", "\[Rule]", "\"\<en\>\""}], ",", 
              RowBox[{
              "\"\<paclet\>\"", "\[Rule]", "\"\<Objects Package\>\""}], ",", 
              RowBox[{"\"\<status\>\"", "\[Rule]", "\"\<\>\""}], ",", 
              RowBox[{"\"\<summary\>\"", "\[Rule]", "\"\<\>\""}], ",", 
              RowBox[{"\"\<synonyms\>\"", "\[Rule]", 
               RowBox[{"{", "}"}]}], ",", 
              RowBox[{"\"\<title\>\"", "\[Rule]", "sym"}], ",", 
              RowBox[{"\"\<type\>\"", "\[Rule]", "\"\<Symbol\>\""}], ",", 
              RowBox[{"\"\<uri\>\"", "\[Rule]", 
               RowBox[{"\"\<Objects/ref/\>\"", "<>", "sym"}]}]}], "}"}]}], 
           ",", 
           RowBox[{"\"\<SearchTextTranslated\>\"", "\[Rule]", "\"\<\>\""}]}], 
          "}"}]}]}], "]"}], ";", "\[IndentingNewLine]", 
      RowBox[{"SetOptions", "[", 
       RowBox[{"nb", ",", 
        RowBox[{"Saveable", "\[Rule]", "True"}]}], "]"}], ";", 
      "\[IndentingNewLine]", 
      RowBox[{"SetOptions", "[", 
       RowBox[{"nb", ",", 
        RowBox[{"StyleDefinitions", "\[Rule]", 
         RowBox[{"FrontEnd`FileName", "[", 
          RowBox[{
           RowBox[{"{", "\"\<Wolfram\>\"", "}"}], ",", 
           "\"\<Reference-editable.nb\>\""}], "]"}]}]}], "]"}], ";", 
      "\[IndentingNewLine]", 
      RowBox[{"NotebookSave", "[", "nb", "]"}], ";", "\[IndentingNewLine]", 
      RowBox[{"NotebookClose", "[", "nb", "]"}], ";"}]}], 
    "\[IndentingNewLine]", "]"}]}], ";"}], "\n", 
 RowBox[{
  RowBox[{
   RowBox[{"UnlockGuide", "[", "file_", "]"}], ":=", 
   RowBox[{"Module", "[", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"sym", ",", "nb"}], "}"}], ",", "\[IndentingNewLine]", 
     RowBox[{
      RowBox[{"sym", "=", 
       RowBox[{"StringDrop", "[", 
        RowBox[{
         RowBox[{"FileNameTake", "[", "file", "]"}], ",", 
         RowBox[{"-", "3"}]}], "]"}]}], ";", "\[IndentingNewLine]", 
      RowBox[{"nb", "=", 
       RowBox[{"NotebookOpen", "[", "file", "]"}]}], ";", 
      "\[IndentingNewLine]", 
      RowBox[{"SetOptions", "[", 
       RowBox[{"nb", ",", 
        RowBox[{"TaggingRules", "\[Rule]", 
         RowBox[{"{", 
          RowBox[{
           RowBox[{"\"\<ModificationHighlight\>\"", "\[Rule]", "False"}], ",", 
           RowBox[{"\"\<Metadata\>\"", "\[Rule]", 
            RowBox[{"{", 
             RowBox[{
              RowBox[{"\"\<context\>\"", "\[Rule]", "\"\<Objects`\>\""}], ",", 
              RowBox[{"\"\<keywords\>\"", "\[Rule]", 
               RowBox[{"{", "}"}]}], ",", 
              RowBox[{"\"\<index\>\"", "\[Rule]", "True"}], ",", 
              RowBox[{
              "\"\<label\>\"", "\[Rule]", "\"\<Objects Package Guide\>\""}], 
              ",", 
              RowBox[{"\"\<language\>\"", "\[Rule]", "\"\<en\>\""}], ",", 
              RowBox[{
              "\"\<paclet\>\"", "\[Rule]", "\"\<Objects Package\>\""}], ",", 
              RowBox[{"\"\<status\>\"", "\[Rule]", "\"\<\>\""}], ",", 
              RowBox[{"\"\<summary\>\"", "\[Rule]", "\"\<\>\""}], ",", 
              RowBox[{"\"\<synonyms\>\"", "\[Rule]", 
               RowBox[{"{", "}"}]}], ",", 
              RowBox[{"\"\<title\>\"", "\[Rule]", "sym"}], ",", 
              RowBox[{"\"\<type\>\"", "\[Rule]", "\"\<Symbol\>\""}], ",", 
              RowBox[{"\"\<uri\>\"", "\[Rule]", 
               RowBox[{"\"\<Objects/guide/\>\"", "<>", "sym"}]}]}], "}"}]}], 
           ",", 
           RowBox[{"\"\<SearchTextTranslated\>\"", "\[Rule]", "\"\<\>\""}]}], 
          "}"}]}]}], "]"}], ";", "\[IndentingNewLine]", 
      RowBox[{"SetOptions", "[", 
       RowBox[{"nb", ",", 
        RowBox[{"Saveable", "\[Rule]", "True"}]}], "]"}], ";", 
      "\[IndentingNewLine]", 
      RowBox[{"SetOptions", "[", 
       RowBox[{"nb", ",", 
        RowBox[{"StyleDefinitions", "\[Rule]", 
         RowBox[{"FrontEnd`FileName", "[", 
          RowBox[{
           RowBox[{"{", "\"\<Wolfram\>\"", "}"}], ",", 
           "\"\<Reference-editable.nb\>\""}], "]"}]}]}], "]"}], ";", 
      "\[IndentingNewLine]", 
      RowBox[{"NotebookSave", "[", "nb", "]"}], ";", "\[IndentingNewLine]", 
      RowBox[{"NotebookClose", "[", "nb", "]"}], ";"}]}], 
    "\[IndentingNewLine]", "]"}]}], ";"}]}], "Input",
 CellChangeTimes->{{3.703909904839645*^9, 3.703909949528728*^9}, {
  3.704163093789351*^9, 3.704163164764791*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{
  "LockSymbol", "[", 
   "\"\</home/kmingulov/Dropbox/\:0414\:043e\:043a\:0443\:043c\:0435\:043d\
\:0442\:044b/\:041f\:0440\:043e\:0435\:043a\:0442\:044b/Mathematica/Objects/\
Documentation/English/ReferencePages/Symbols/ObjectsProfile.nb\>\"", "]"}], 
  ";"}]], "Input",
 CellChangeTimes->{{3.703910367109932*^9, 3.70391036854097*^9}, 
   3.703916184851605*^9, {3.704163172719391*^9, 3.70416317416149*^9}, {
   3.713238524351985*^9, 3.713238545117302*^9}, 3.713239233498685*^9, {
   3.713239267552538*^9, 3.713239274701517*^9}, 3.71324003189146*^9, {
   3.713240090595806*^9, 3.713240096177456*^9}, {3.7132401498477182`*^9, 
   3.71324017255186*^9}, {3.713240460858902*^9, 3.713240461711843*^9}, {
   3.713240495460143*^9, 3.7132404961262283`*^9}, {3.713487098244884*^9, 
   3.713487107377879*^9}, {3.71348721389811*^9, 3.7134872141236057`*^9}, {
   3.714800096444868*^9, 3.714800100735656*^9}, {3.714801676064917*^9, 
   3.714801676296831*^9}, {3.7148017477540703`*^9, 3.7148017485926857`*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{
  "LockGuide", "[", 
   "\"\</home/kmingulov/Dropbox/\:0414\:043e\:043a\:0443\:043c\:0435\:043d\
\:0442\:044b/\:041f\:0440\:043e\:0435\:043a\:0442\:044b/Mathematica/Objects/\
Documentation/English/Guides/ObjectsPackage.nb\>\"", "]"}], ";"}]], "Input",
 CellChangeTimes->{{3.704163180113743*^9, 3.704163182097624*^9}, {
   3.713240539366263*^9, 3.713240543672789*^9}, 3.713240853686283*^9, {
   3.7134875205226803`*^9, 3.71348752089113*^9}, 3.713487584497531*^9, {
   3.714801756729836*^9, 3.714801757073677*^9}, 3.714801874386416*^9}]
},
WindowSize->{1918, 990},
WindowMargins->{{-9, Automatic}, {6, Automatic}},
FrontEndVersion->"10.4 for Linux x86 (64-bit) (April 11, 2016)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 96, 1, 37, "Input"],
Cell[657, 23, 189, 4, 37, "Input"],
Cell[849, 29, 661, 17, 66, "Input"],
Cell[1513, 48, 11070, 246, 1466, "Input"],
Cell[12586, 296, 1031, 17, 37, "Input"],
Cell[13620, 315, 569, 10, 37, "Input"]
}
]
*)


(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 10.4' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     11440,        452]
NotebookOptionsPosition[      8388,        349]
NotebookOutlinePosition[      9487,        384]
CellTagsIndexPosition[      9403,        379]
WindowTitle->InstanceQ
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[GridBox[{
   {Cell["OBJECTS PACKAGE SYMBOL", "PacletNameCell"]}
  }]], "AnchorBarGrid",
 CellID->1],

Cell[TextData[{
 Cell["InstanceQ", "ObjectName"],
 Cell[BoxData[
  InterpretationBox[
   StyleBox[
    GraphicsBox[{},
     BaselinePosition->Baseline,
     ImageSize->{8, 0}],
    CacheGraphics->False],
   Spacer[8]]]]
}], "ObjectNameGrid"],

Cell[CellGroupData[{

Cell[BoxData[GridBox[{
   {"", Cell[TextData[{
     Cell[BoxData[
      RowBox[{
       TemplateBox[{Cell[
          TextData["InstanceQ"]],"paclet:Objects/ref/InstanceQ"},
        "RefLink",
        BaseStyle->"InlineFormula"], "[", 
       RowBox[{
        StyleBox["obj", "TI"], ",", 
        StyleBox["class", "TI"]}], "]"}]], "InlineFormula"],
     " \[LineSeparator]checks whether ",
     StyleBox["obj", "TI"],
     " is an instance of ",
     StyleBox["class", "TI"],
     "."
    }]]}
  }]], "Usage",
 GridBoxOptions->{
 GridBoxBackground->{
  "Columns" -> {{None}}, "ColumnsIndexed" -> {}, 
   "Rows" -> {None, None, {None}}, "RowsIndexed" -> {}}},
 CellID->17713],

Cell[CellGroupData[{

Cell[TextData[Cell[BoxData[
 ButtonBox[Cell[TextData[{
   Cell[BoxData[
    InterpretationBox[
     StyleBox[
      GraphicsBox[{},
       BaselinePosition->Baseline,
       ImageSize->{6, 0}],
      CacheGraphics->False],
     Spacer[6]]]],
   "Details"
  }], "NotesFrameText"],
  Appearance->{Automatic, None},
  BaseStyle->None,
  ButtonFunction:>(FrontEndExecute[{
     FrontEnd`SelectionMove[
      FrontEnd`SelectedNotebook[], All, ButtonCell], 
     FrontEndToken["OpenCloseGroup"], 
     FrontEnd`SelectionMove[
      FrontEnd`SelectedNotebook[], After, CellContents]}]& ),
  Evaluator->None,
  Method->"Preemptive"]]]], "NotesSection",
 WholeCellGroupOpener->True,
 CellGroupingRules->{"SectionGrouping", 50},
 CellID->75457274],

Cell["", "SectionHeaderSpacer"],

Cell[TextData[{
 "To use this function, you first need to load the ",
 ButtonBox["Objects Package",
  BaseStyle->"Link",
  ButtonData->"paclet:Objects/guide/ObjectsPackage"],
 "."
}], "Notes",
 CellID->718278630],

Cell[TextData[{
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["InstanceQ"]],"paclet:Objects/ref/InstanceQ"},
   "RefLink",
   BaseStyle->"InlineFormula"]], "InlineFormula"],
 " returns True, even if ",
 StyleBox["obj", "TI"],
 " is an instance of some class, which inherits ",
 StyleBox["parent", "TI"],
 "."
}], "Notes",
 CellID->136833138],

Cell[TextData[{
 "In order to check, whether ",
 StyleBox["obj", "TI"],
 " is a ",
 StyleBox["direct",
  FontWeight->"Bold"],
 " instance of ",
 StyleBox["class", "TI"],
 ", use ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["DirectInstanceQ"]],"paclet:Objects/ref/DirectInstanceQ"},
   "RefLink",
   BaseStyle->"InlineFormula"]], "InlineFormula"],
 " instead."
}], "Notes",
 CellID->994190625],

Cell["", "SectionFooterSpacer"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[{
 Cell[BoxData[
  InterpretationBox[
   StyleBox[
    GraphicsBox[{},
     BaselinePosition->Baseline,
     ImageSize->{6, 0}],
    CacheGraphics->False],
   Spacer[6]]]],
 "Examples"
}], "PrimaryExamplesSection",
 WholeCellGroupOpener->True,
 CellTags->"PrimaryExamplesSection",
 CellID->1195052388],

Cell[CellGroupData[{

Cell["Basic example", "ExampleSection", "ExampleSection",
 WholeCellGroupOpener->True,
 CellID->1978418372],

Cell[BoxData[
 RowBox[{"<<", "Objects`"}]], "Input",
 CellChangeTimes->{{3.703571888770885*^9, 3.703571890101639*^9}},
 CellLabel->"In[1]:=",
 CellID->2007368699],

Cell[TextData[{
 "Create several simple classes with ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["CNew"]],"paclet:Objects/ref/CNew"},
   "RefLink",
   BaseStyle->"InlineFormula"]], "InlineFormula"],
 ":"
}], "ExampleText",
 CellID->477905686],

Cell[BoxData[
 RowBox[{
  RowBox[{"CNew", "[", 
   RowBox[{"ClassA", ",", 
    RowBox[{"{", "}"}], ",", 
    RowBox[{"{", "}"}]}], "]"}], ";"}]], "Input",
 CellLabel->"In[2]:=",
 CellID->340848625],

Cell[BoxData[
 RowBox[{
  RowBox[{"CNew", "[", 
   RowBox[{"ClassB", ",", 
    RowBox[{"{", "}"}], ",", 
    RowBox[{"{", "}"}]}], "]"}], ";"}]], "Input",
 CellLabel->"In[3]:=",
 CellID->1835678537],

Cell[BoxData[
 RowBox[{
  RowBox[{"CNew", "[", 
   RowBox[{
    RowBox[{"ClassC", "\[Rule]", "ClassA"}], ",", 
    RowBox[{"{", "}"}], ",", 
    RowBox[{"{", "}"}]}], "]"}], ";"}]], "Input",
 CellLabel->"In[4]:=",
 CellID->1888323599],

Cell[TextData[{
 "Create an instance of ",
 StyleBox["ClassC", "InlineFormula"],
 ":"
}], "ExampleText",
 CellID->1200443100],

Cell[BoxData[
 RowBox[{
  RowBox[{"ONew", "[", 
   RowBox[{"obj", ",", "ClassC"}], "]"}], ";"}]], "Input",
 CellChangeTimes->{{3.704086550806526*^9, 3.704086555444728*^9}, {
  3.704087891448636*^9, 3.704087893195826*^9}},
 CellLabel->"In[5]:=",
 CellID->42436961],

Cell[TextData[{
 "Perform checks on ",
 StyleBox["obj", "InlineFormula"],
 ":"
}], "ExampleText",
 CellID->852042784],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"InstanceQ", "[", 
    RowBox[{"obj", ",", "#"}], "]"}], "&"}], "/@", 
  RowBox[{"{", 
   RowBox[{"ClassA", ",", "ClassB", ",", "ClassC"}], "}"}]}]], "Input",
 CellChangeTimes->{{3.703765692007699*^9, 3.703765703262608*^9}},
 CellLabel->"In[6]:=",
 CellID->501175458],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"True", ",", "False", ",", "True"}], "}"}]], "Output",
 CellLabel->"Out[6]=",
 CellID->1977074138]
}, Open  ]],

Cell[BoxData[
 RowBox[{
  RowBox[{"ODelete", "[", "obj", "]"}], ";"}]], "Input",
 CellLabel->"In[7]:=",
 CellID->1247145880],

Cell[BoxData[
 RowBox[{
  RowBox[{"CDelete", "[", 
   RowBox[{"ClassA", ",", "ClassB", ",", "ClassC"}], "]"}], ";"}]], "Input",
 CellLabel->"In[8]:=",
 CellID->682619210]
}, Open  ]],

Cell["", "SectionFooterSpacer",
 CellID->2115137253]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[{
 Cell[BoxData[
  InterpretationBox[
   StyleBox[
    GraphicsBox[{},
     BaselinePosition->Baseline,
     ImageSize->{6, 0}],
    CacheGraphics->False],
   Spacer[6]]]],
 "See Also"
}], "SeeAlsoSection",
 WholeCellGroupOpener->True,
 CellID->20994],

Cell[TextData[{
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["ONew"]],"paclet:Objects/ref/ONew"},
   "RefLink",
   BaseStyle->{"InlineFormula", FontFamily -> "Verdana"}]], "InlineFormula"],
 "\[NonBreakingSpace]",
 StyleBox["\[MediumSpace]\[FilledVerySmallSquare]\[MediumSpace]", 
  "InlineSeparator"],
 " ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["ODelete"]],"paclet:Objects/ref/ODelete"},
   "RefLink",
   BaseStyle->{"InlineFormula", FontFamily -> "Verdana"}]], "InlineFormula"],
 "\[NonBreakingSpace]",
 StyleBox["\[MediumSpace]\[FilledVerySmallSquare]\[MediumSpace]", 
  "InlineSeparator"],
 " ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["ObjectQ"]],"paclet:Objects/ref/ObjectQ"},
   "RefLink",
   BaseStyle->{"InlineFormula", FontFamily -> "Verdana"}]], "InlineFormula"],
 "\[NonBreakingSpace]",
 StyleBox["\[MediumSpace]\[FilledVerySmallSquare]\[MediumSpace]", 
  "InlineSeparator"],
 " ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["DirectInstanceQ"]],"paclet:Objects/ref/DirectInstanceQ"},
   "RefLink",
   BaseStyle->{"InlineFormula", FontFamily -> "Verdana"}]], "InlineFormula"]
}], "SeeAlso",
 CellID->18286]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[{
 Cell[BoxData[
  InterpretationBox[
   StyleBox[
    GraphicsBox[{},
     BaselinePosition->Baseline,
     ImageSize->{6, 0}],
    CacheGraphics->False],
   Spacer[6]]]],
 "Related Guides"
}], "MoreAboutSection",
 WholeCellGroupOpener->True,
 CellID->1973046713],

Cell["", "SectionHeaderSpacer"],

Cell[BoxData[
 TemplateBox[{Cell[
    TextData["Objects Package"]],"paclet:Objects/guide/ObjectsPackage"},
  "RefLink",
  BaseStyle->"MoreAbout"]], "MoreAbout",
 CellID->1788271703]
}, Open  ]],

Cell[" ", "FooterCell"]
},
Saveable->False,
ScreenStyleEnvironment->"Working",
WindowSize->{1918, 1005},
WindowMargins->{{-9, Automatic}, {Automatic, -8}},
WindowTitle->"InstanceQ",
TaggingRules->{
 "ModificationHighlight" -> False, 
  "Metadata" -> {
   "context" -> "Objects`", "keywords" -> {}, "index" -> True, "label" -> 
    "Objects Package Symbol", "language" -> "en", "paclet" -> 
    "Objects Package", "status" -> "", "summary" -> "", "synonyms" -> {}, 
    "title" -> "InstanceQ", "type" -> "Symbol", "uri" -> 
    "Objects/ref/InstanceQ"}, "SearchTextTranslated" -> "", "LinkTrails" -> 
  ""},
FrontEndVersion->"10.4 for Linux x86 (64-bit) (April 11, 2016)",
StyleDefinitions->FrontEnd`FileName[{"Wolfram"}, "Reference.nb", 
  CharacterEncoding -> "UTF-8"]
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{
 "PrimaryExamplesSection"->{
  Cell[3481, 141, 315, 13, 39, "PrimaryExamplesSection",
   CellTags->"PrimaryExamplesSection",
   CellID->1195052388]}
 }
*)
(*CellTagsIndex
CellTagsIndex->{
 {"PrimaryExamplesSection", 9259, 372}
 }
*)
(*NotebookFileOutline
Notebook[{
Cell[581, 21, 112, 3, 43, "AnchorBarGrid",
 CellID->1],
Cell[696, 26, 241, 10, 61, "ObjectNameGrid"],
Cell[CellGroupData[{
Cell[962, 40, 674, 22, 96, "Usage",
 CellID->17713],
Cell[CellGroupData[{
Cell[1661, 66, 737, 24, 40, "NotesSection",
 CellGroupingRules->{"SectionGrouping", 50},
 CellID->75457274],
Cell[2401, 92, 31, 0, 14, "SectionHeaderSpacer"],
Cell[2435, 94, 212, 7, 36, "Notes",
 CellID->718278630],
Cell[2650, 103, 345, 12, 39, "Notes",
 CellID->136833138],
Cell[2998, 117, 400, 16, 39, "Notes",
 CellID->994190625],
Cell[3401, 135, 31, 0, 29, "SectionFooterSpacer"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[3481, 141, 315, 13, 39, "PrimaryExamplesSection",
 CellTags->"PrimaryExamplesSection",
 CellID->1195052388],
Cell[CellGroupData[{
Cell[3821, 158, 107, 2, 33, "ExampleSection",
 CellID->1978418372],
Cell[3931, 162, 162, 4, 33, "Input",
 CellID->2007368699],
Cell[4096, 168, 251, 9, 43, "ExampleText",
 CellID->477905686],
Cell[4350, 179, 197, 7, 33, "Input",
 CellID->340848625],
Cell[4550, 188, 198, 7, 33, "Input",
 CellID->1835678537],
Cell[4751, 197, 234, 8, 33, "Input",
 CellID->1888323599],
Cell[4988, 207, 125, 5, 41, "ExampleText",
 CellID->1200443100],
Cell[5116, 214, 263, 7, 33, "Input",
 CellID->42436961],
Cell[5382, 223, 117, 5, 41, "ExampleText",
 CellID->852042784],
Cell[CellGroupData[{
Cell[5524, 232, 313, 9, 33, "Input",
 CellID->501175458],
Cell[5840, 243, 137, 4, 42, "Output",
 CellID->1977074138]
}, Open  ]],
Cell[5992, 250, 124, 4, 33, "Input",
 CellID->1247145880],
Cell[6119, 256, 170, 5, 33, "Input",
 CellID->682619210]
}, Open  ]],
Cell[6304, 264, 52, 1, 29, "SectionFooterSpacer",
 CellID->2115137253]
}, Open  ]],
Cell[CellGroupData[{
Cell[6393, 270, 265, 12, 39, "SeeAlsoSection",
 CellID->20994],
Cell[6661, 284, 1152, 34, 67, "SeeAlso",
 CellID->18286]
}, Open  ]],
Cell[CellGroupData[{
Cell[7850, 323, 278, 12, 39, "MoreAboutSection",
 CellID->1973046713],
Cell[8131, 337, 31, 0, 14, "SectionHeaderSpacer"],
Cell[8165, 339, 181, 5, 26, "MoreAbout",
 CellID->1788271703]
}, Open  ]],
Cell[8361, 347, 23, 0, 47, "FooterCell"]
}
]
*)


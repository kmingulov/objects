(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 10.4' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     11175,        427]
NotebookOptionsPosition[      8187,        326]
NotebookOutlinePosition[      9301,        361]
CellTagsIndexPosition[      9217,        356]
WindowTitle->AbstractClassQ
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[GridBox[{
   {Cell["OBJECTS PACKAGE SYMBOL", "PacletNameCell"]}
  }]], "AnchorBarGrid",
 CellID->1],

Cell[TextData[{
 Cell["AbstractClassQ", "ObjectName"],
 Cell[BoxData[
  InterpretationBox[
   StyleBox[
    GraphicsBox[{},
     BaselinePosition->Baseline,
     ImageSize->{8, 0}],
    CacheGraphics->False],
   Spacer[8]]]]
}], "ObjectNameGrid"],

Cell[CellGroupData[{

Cell[BoxData[GridBox[{
   {"", Cell[TextData[{
     Cell[BoxData[
      RowBox[{
       TemplateBox[{Cell[
          TextData["AbstractClassQ"]],"paclet:Objects/ref/AbstractClassQ"},
        "RefLink",
        BaseStyle->"InlineFormula"], "[", 
       StyleBox["class", "TI"], "]"}]], "InlineFormula"],
     " \[LineSeparator]checks whether ",
     StyleBox["class", "TI"],
     " is abstract."
    }]]}
  }]], "Usage",
 GridBoxOptions->{
 GridBoxBackground->{
  "Columns" -> {{None}}, "ColumnsIndexed" -> {}, 
   "Rows" -> {None, None, {None}}, "RowsIndexed" -> {}}},
 CellID->17713],

Cell[CellGroupData[{

Cell[TextData[Cell[BoxData[
 ButtonBox[Cell[TextData[{
   Cell[BoxData[
    InterpretationBox[
     StyleBox[
      GraphicsBox[{},
       BaselinePosition->Baseline,
       ImageSize->{6, 0}],
      CacheGraphics->False],
     Spacer[6]]]],
   "Details"
  }], "NotesFrameText"],
  Appearance->{Automatic, None},
  BaseStyle->None,
  ButtonFunction:>(FrontEndExecute[{
     FrontEnd`SelectionMove[
      FrontEnd`SelectedNotebook[], All, ButtonCell], 
     FrontEndToken["OpenCloseGroup"], 
     FrontEnd`SelectionMove[
      FrontEnd`SelectedNotebook[], After, CellContents]}]& ),
  Evaluator->None,
  Method->"Preemptive"]]]], "NotesSection",
 WholeCellGroupOpener->True,
 CellGroupingRules->{"SectionGrouping", 50},
 CellID->75457274],

Cell["", "SectionHeaderSpacer"],

Cell[TextData[{
 "To use this function, you first need to load the ",
 ButtonBox["Objects Package",
  BaseStyle->"Link",
  ButtonData->"paclet:Objects/guide/ObjectsPackage"],
 "."
}], "Notes",
 CellID->718278630],

Cell[TextData[{
 "When called on symbols and atoms, which are not classes, ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["AbstractClassQ"]],"paclet:Objects/ref/AbstractClassQ"},
   "RefLink",
   BaseStyle->"InlineFormula"]], "InlineFormula"],
 " returns ",
 StyleBox["False", "InlineFormula"],
 "."
}], "Notes",
 CellID->136833138],

Cell["", "SectionFooterSpacer"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[{
 Cell[BoxData[
  InterpretationBox[
   StyleBox[
    GraphicsBox[{},
     BaselinePosition->Baseline,
     ImageSize->{6, 0}],
    CacheGraphics->False],
   Spacer[6]]]],
 "Examples"
}], "PrimaryExamplesSection",
 WholeCellGroupOpener->True,
 CellTags->"PrimaryExamplesSection",
 CellID->1195052388],

Cell[CellGroupData[{

Cell["Basic example", "ExampleSection", "ExampleSection",
 WholeCellGroupOpener->True,
 CellID->1978418372],

Cell[BoxData[
 RowBox[{"<<", "Objects`"}]], "Input",
 CellChangeTimes->{{3.703571888770885*^9, 3.703571890101639*^9}},
 CellLabel->"In[1]:=",
 CellID->2007368699],

Cell[TextData[{
 "Create several simple classes with ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["CNew"]],"paclet:Objects/ref/CNew"},
   "RefLink",
   BaseStyle->"InlineFormula"]], "InlineFormula"],
 ":"
}], "ExampleText",
 CellID->477905686],

Cell[BoxData[
 RowBox[{
  RowBox[{"CNew", "[", 
   RowBox[{"ClassA", ",", 
    RowBox[{"{", "}"}], ",", "\[IndentingNewLine]", 
    RowBox[{"{", "\[IndentingNewLine]", 
     RowBox[{"$Method", "[", 
      RowBox[{"method", ",", 
       RowBox[{"{", "this", "}"}], ",", "\[IndentingNewLine]", "$Abstract"}], 
      "\[IndentingNewLine]", "]"}], "\[IndentingNewLine]", "}"}]}], 
   "\[IndentingNewLine]", "]"}], ";"}]], "Input",
 CellLabel->"In[2]:=",
 CellID->340848625],

Cell[BoxData[
 RowBox[{
  RowBox[{"CNew", "[", 
   RowBox[{"ClassB", ",", 
    RowBox[{"{", "}"}], ",", "\[IndentingNewLine]", 
    RowBox[{"{", "\[IndentingNewLine]", 
     RowBox[{"$Method", "[", 
      RowBox[{"method", ",", 
       RowBox[{"{", "this", "}"}], ",", "\[IndentingNewLine]", "Null"}], 
      "\[IndentingNewLine]", "]"}], "\[IndentingNewLine]", "}"}]}], 
   "\[IndentingNewLine]", "]"}], ";"}]], "Input",
 CellLabel->"In[3]:=",
 CellID->1835678537],

Cell["Check which of them is abstract:", "ExampleText",
 CellID->1200443100],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"AbstractClassQ", "/@", 
  RowBox[{"{", 
   RowBox[{"ClassA", ",", "ClassB"}], "}"}]}]], "Input",
 CellLabel->"In[4]:=",
 CellID->1843771792],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"True", ",", "False"}], "}"}]], "Output",
 CellLabel->"Out[4]=",
 CellID->1689354571]
}, Open  ]],

Cell[TextData[{
 "For random symbols and atoms, ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["AbstractClassQ"]],"paclet:Objects/ref/AbstractClassQ"},
   "RefLink",
   BaseStyle->"InlineFormula"]], "InlineFormula"],
 " returns ",
 StyleBox["False", "InlineFormula"],
 ":"
}], "ExampleText",
 CellID->68246542],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"AbstractClassQ", "/@", 
  RowBox[{"{", 
   RowBox[{"Sin", ",", "2", ",", "\[Pi]", ",", "Arg"}], "}"}]}]], "Input",
 CellChangeTimes->{{3.704157921403233*^9, 3.704157924462029*^9}},
 CellLabel->"In[5]:=",
 CellID->1439048208],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"False", ",", "False", ",", "False", ",", "False"}], 
  "}"}]], "Output",
 CellLabel->"Out[5]=",
 CellID->991229724]
}, Open  ]],

Cell[BoxData[
 RowBox[{"CDelete", "[", 
  RowBox[{"ClassA", ",", "ClassB"}], "]"}]], "Input",
 CellLabel->"In[6]:=",
 CellID->1247145880]
}, Open  ]],

Cell["", "SectionFooterSpacer",
 CellID->2115137253]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[{
 Cell[BoxData[
  InterpretationBox[
   StyleBox[
    GraphicsBox[{},
     BaselinePosition->Baseline,
     ImageSize->{6, 0}],
    CacheGraphics->False],
   Spacer[6]]]],
 "See Also"
}], "SeeAlsoSection",
 WholeCellGroupOpener->True,
 CellID->20994],

Cell[TextData[{
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["CNew"]],"paclet:Objects/ref/CNew"},
   "RefLink",
   BaseStyle->{"InlineFormula", FontFamily -> "Verdana"}]], "InlineFormula"],
 "\[NonBreakingSpace]",
 StyleBox["\[MediumSpace]\[FilledVerySmallSquare]\[MediumSpace]", 
  "InlineSeparator"],
 " ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["CDelete"]],"paclet:Objects/ref/CDelete"},
   "RefLink",
   BaseStyle->{"InlineFormula", FontFamily -> "Verdana"}]], "InlineFormula"],
 "\[NonBreakingSpace]",
 StyleBox["\[MediumSpace]\[FilledVerySmallSquare]\[MediumSpace]", 
  "InlineSeparator"],
 " ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["ClassQ"]],"paclet:Objects/ref/ClassQ"},
   "RefLink",
   BaseStyle->{"InlineFormula", FontFamily -> "Verdana"}]], "InlineFormula"],
 "\[NonBreakingSpace]",
 StyleBox["\[MediumSpace]\[FilledVerySmallSquare]\[MediumSpace]", 
  "InlineSeparator"],
 " ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["ObjectQ"]],"paclet:Objects/ref/ObjectQ"},
   "RefLink",
   BaseStyle->{"InlineFormula", FontFamily -> "Verdana"}]], "InlineFormula"]
}], "SeeAlso",
 CellID->18286]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[{
 Cell[BoxData[
  InterpretationBox[
   StyleBox[
    GraphicsBox[{},
     BaselinePosition->Baseline,
     ImageSize->{6, 0}],
    CacheGraphics->False],
   Spacer[6]]]],
 "Related Guides"
}], "MoreAboutSection",
 WholeCellGroupOpener->True,
 CellID->1973046713],

Cell["", "SectionHeaderSpacer"],

Cell[BoxData[
 TemplateBox[{Cell[
    TextData["Objects Package"]],"paclet:Objects/guide/ObjectsPackage"},
  "RefLink",
  BaseStyle->"MoreAbout"]], "MoreAbout",
 CellID->1788271703]
}, Open  ]],

Cell[" ", "FooterCell"]
},
Saveable->False,
ScreenStyleEnvironment->"Working",
WindowSize->{1918, 1005},
WindowMargins->{{-9, Automatic}, {Automatic, -8}},
WindowTitle->"AbstractClassQ",
TaggingRules->{
 "ModificationHighlight" -> False, 
  "Metadata" -> {
   "context" -> "Objects`", "keywords" -> {}, "index" -> True, "label" -> 
    "Objects Package Symbol", "language" -> "en", "paclet" -> 
    "Objects Package", "status" -> "", "summary" -> "", "synonyms" -> {}, 
    "title" -> "AbstractClassQ", "type" -> "Symbol", "uri" -> 
    "Objects/ref/AbstractClassQ"}, "SearchTextTranslated" -> "", "LinkTrails" -> 
  ""},
FrontEndVersion->"10.4 for Linux x86 (64-bit) (April 11, 2016)",
StyleDefinitions->FrontEnd`FileName[{"Wolfram"}, "Reference.nb", 
  CharacterEncoding -> "UTF-8"]
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{
 "PrimaryExamplesSection"->{
  Cell[2991, 118, 315, 13, 39, "PrimaryExamplesSection",
   CellTags->"PrimaryExamplesSection",
   CellID->1195052388]}
 }
*)
(*CellTagsIndex
CellTagsIndex->{
 {"PrimaryExamplesSection", 9073, 349}
 }
*)
(*NotebookFileOutline
Notebook[{
Cell[586, 21, 112, 3, 43, "AnchorBarGrid",
 CellID->1],
Cell[701, 26, 246, 10, 61, "ObjectNameGrid"],
Cell[CellGroupData[{
Cell[972, 40, 584, 18, 96, "Usage",
 CellID->17713],
Cell[CellGroupData[{
Cell[1581, 62, 737, 24, 40, "NotesSection",
 CellGroupingRules->{"SectionGrouping", 50},
 CellID->75457274],
Cell[2321, 88, 31, 0, 14, "SectionHeaderSpacer"],
Cell[2355, 90, 212, 7, 36, "Notes",
 CellID->718278630],
Cell[2570, 99, 338, 11, 39, "Notes",
 CellID->136833138],
Cell[2911, 112, 31, 0, 29, "SectionFooterSpacer"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[2991, 118, 315, 13, 39, "PrimaryExamplesSection",
 CellTags->"PrimaryExamplesSection",
 CellID->1195052388],
Cell[CellGroupData[{
Cell[3331, 135, 107, 2, 33, "ExampleSection",
 CellID->1978418372],
Cell[3441, 139, 162, 4, 33, "Input",
 CellID->2007368699],
Cell[3606, 145, 251, 9, 43, "ExampleText",
 CellID->477905686],
Cell[3860, 156, 469, 12, 172, "Input",
 CellID->340848625],
Cell[4332, 170, 465, 12, 172, "Input",
 CellID->1835678537],
Cell[4800, 184, 76, 1, 40, "ExampleText",
 CellID->1200443100],
Cell[CellGroupData[{
Cell[4901, 189, 164, 5, 33, "Input",
 CellID->1843771792],
Cell[5068, 196, 124, 4, 42, "Output",
 CellID->1689354571]
}, Open  ]],
Cell[5207, 203, 316, 11, 43, "ExampleText",
 CellID->68246542],
Cell[CellGroupData[{
Cell[5548, 218, 248, 6, 33, "Input",
 CellID->1439048208],
Cell[5799, 226, 155, 5, 42, "Output",
 CellID->991229724]
}, Open  ]],
Cell[5969, 234, 137, 4, 33, "Input",
 CellID->1247145880]
}, Open  ]],
Cell[6121, 241, 52, 1, 29, "SectionFooterSpacer",
 CellID->2115137253]
}, Open  ]],
Cell[CellGroupData[{
Cell[6210, 247, 265, 12, 39, "SeeAlsoSection",
 CellID->20994],
Cell[6478, 261, 1134, 34, 67, "SeeAlso",
 CellID->18286]
}, Open  ]],
Cell[CellGroupData[{
Cell[7649, 300, 278, 12, 39, "MoreAboutSection",
 CellID->1973046713],
Cell[7930, 314, 31, 0, 14, "SectionHeaderSpacer"],
Cell[7964, 316, 181, 5, 26, "MoreAbout",
 CellID->1788271703]
}, Open  ]],
Cell[8160, 324, 23, 0, 47, "FooterCell"]
}
]
*)


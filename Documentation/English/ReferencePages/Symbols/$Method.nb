(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 10.4' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     20703,        737]
NotebookOptionsPosition[     16489,        593]
NotebookOutlinePosition[     17578,        627]
CellTagsIndexPosition[     17493,        622]
WindowTitle->$Method
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[GridBox[{
   {Cell["OBJECTS PACKAGE SYMBOL", "PacletNameCell"]}
  }]], "AnchorBarGrid",
 CellID->1],

Cell[TextData[{
 Cell["$Method", "ObjectName"],
 Cell[BoxData[
  InterpretationBox[
   StyleBox[
    GraphicsBox[{},
     BaselinePosition->Baseline,
     ImageSize->{8, 0}],
    CacheGraphics->False],
   Spacer[8]]]]
}], "ObjectNameGrid"],

Cell[CellGroupData[{

Cell[BoxData[GridBox[{
   {"", Cell[TextData[{
     Cell[BoxData[
      TemplateBox[{Cell[
         TextData["$Method"]],"paclet:Objects/ref/$Method"},
       "RefLink",
       BaseStyle->"InlineFormula"]], "InlineFormula"],
     " \[LineSeparator]is a keyword used for constructor's definition."
    }]]}
  }]], "Usage",
 GridBoxOptions->{
 GridBoxBackground->{
  "Columns" -> {{None}}, "ColumnsIndexed" -> {}, 
   "Rows" -> {None, None, {None}}, "RowsIndexed" -> {}}},
 CellID->17713],

Cell[CellGroupData[{

Cell[TextData[Cell[BoxData[
 ButtonBox[Cell[TextData[{
   Cell[BoxData[
    InterpretationBox[
     StyleBox[
      GraphicsBox[{},
       BaselinePosition->Baseline,
       ImageSize->{6, 0}],
      CacheGraphics->False],
     Spacer[6]]]],
   "Details"
  }], "NotesFrameText"],
  Appearance->{Automatic, None},
  BaseStyle->None,
  ButtonFunction:>(FrontEndExecute[{
     FrontEnd`SelectionMove[
      FrontEnd`SelectedNotebook[], All, ButtonCell], 
     FrontEndToken["OpenCloseGroup"], 
     FrontEnd`SelectionMove[
      FrontEnd`SelectedNotebook[], After, CellContents]}]& ),
  Evaluator->None,
  Method->"Preemptive"]]]], "NotesSection",
 WholeCellGroupOpener->True,
 CellGroupingRules->{"SectionGrouping", 50},
 CellID->75457274],

Cell["", "SectionHeaderSpacer"],

Cell[TextData[{
 "To use this keyword, you first need to load the ",
 ButtonBox["Objects Package",
  BaseStyle->"Link",
  ButtonData->"paclet:Objects/guide/ObjectsPackage"],
 "."
}], "Notes",
 CellID->718278630],

Cell[TextData[{
 "A method is formatted with the ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["$Method"]],"paclet:Objects/ref/$Method"},
   "RefLink",
   BaseStyle->"InlineFormula"]], "InlineFormula"],
 " keyword in the following ways:"
}], "Notes",
 CellID->622894829],

Cell[BoxData[GridBox[{
   {Cell["      ", "TableRowIcon"], 
    RowBox[{"$Method", "[", 
     RowBox[{"name", ",", 
      RowBox[{"{", 
       RowBox[{"this", ",", "arg1", ",", "arg2", ",", "\[Ellipsis]"}], "}"}], 
      ",", "\[IndentingNewLine]", 
      RowBox[{"(*", " ", 
       RowBox[{"Method", " ", 
        RowBox[{"body", "."}]}], " ", "*)"}], "\[IndentingNewLine]", 
      "\[Ellipsis]"}], "\[IndentingNewLine]", "]"}], Cell[TextData[{
     "Define a method with given ",
     StyleBox["name", "TI"],
     " and given ",
     StyleBox["body", "TI"],
     ".\n",
     StyleBox["name", "TI"],
     " has to be a ",
     Cell[BoxData[
      TemplateBox[{Cell[
         TextData["Symbol"]],"ref/Symbol"},
       "RefLink",
       BaseStyle->"InlineFormula"]], "InlineFormula"],
     " with no ",
     Cell[BoxData[
      TemplateBox[{Cell[
         TextData["Down"]],"ref/DownValues"},
       "RefLink",
       BaseStyle->"InlineFormula"]], "InlineFormula"],
     "- and ",
     Cell[BoxData[
      TemplateBox[{Cell[
         TextData["UpValues"]],"ref/UpValues"},
       "RefLink",
       BaseStyle->"InlineFormula"]], "InlineFormula"],
     " assigned to it.\nIn the ",
     StyleBox["body", "TI"],
     " definition ",
     StyleBox["this", "TI"],
     " is an object, to which method is applied."
    }], "TableText"]},
   {Cell["      ", "TableRowIcon"], 
    RowBox[{"$Method", "[", 
     RowBox[{"name", ",", 
      RowBox[{"{", 
       RowBox[{"this", ",", "arg1", ",", "arg2", ",", "\[Ellipsis]"}], "}"}], 
      ",", "\[IndentingNewLine]", 
      RowBox[{"(*", " ", 
       RowBox[{"Before", " ", 
        RowBox[{"method", "."}]}], " ", "*)"}], "\[IndentingNewLine]", 
      "\[Ellipsis]", ",", "\[IndentingNewLine]", 
      RowBox[{"(*", " ", 
       RowBox[{"Method", " ", 
        RowBox[{"body", "."}]}], " ", "*)"}], "\[IndentingNewLine]", 
      "$Abstract", ",", "\[IndentingNewLine]", 
      RowBox[{"(*", " ", 
       RowBox[{"After", " ", 
        RowBox[{"method", "."}]}], " ", "*)"}], "\[IndentingNewLine]", 
      "\[Ellipsis]"}], "\[IndentingNewLine]", "]"}], Cell[TextData[{
     "Define a method with given ",
     StyleBox["name", "TI"],
     " and given ",
     StyleBox["body", "TI"],
     " and with pre-method ",
     StyleBox["before", "TI"],
     " and post-method ",
     StyleBox["after", "TI"],
     ".\n",
     StyleBox["name", "TI"],
     " has to be a ",
     Cell[BoxData[
      TemplateBox[{Cell[
         TextData["Symbol"]],"ref/Symbol"},
       "RefLink",
       BaseStyle->"InlineFormula"]], "InlineFormula"],
     " with no ",
     Cell[BoxData[
      TemplateBox[{Cell[
         TextData["Down"]],"ref/DownValues"},
       "RefLink",
       BaseStyle->"InlineFormula"]], "InlineFormula"],
     "- and ",
     Cell[BoxData[
      TemplateBox[{Cell[
         TextData["UpValues"]],"ref/UpValues"},
       "RefLink",
       BaseStyle->"InlineFormula"]], "InlineFormula"],
     " assigned to it.\nThe pre-method ",
     StyleBox["before", "TI"],
     " will be triggered before the main body and the post-method \[LongDash] \
after it.\nNote that only abstract methods (what is denoted with ",
     Cell[BoxData[
      TemplateBox[{Cell[
         TextData["$Abstract"]],"paclet:Objects/ref/$Abstract"},
       "RefLink",
       BaseStyle->"InlineFormula"]], "InlineFormula"],
     " keyword) can have ",
     StyleBox["before", "TI"],
     " and ",
     StyleBox["after", "TI"],
     "."
    }], "TableText"]},
   {Cell["      ", "TableRowIcon"], 
    RowBox[{"$Method", "[", 
     RowBox[{"name", ",", 
      RowBox[{"{", 
       RowBox[{"this", ",", "arg1", ",", 
        RowBox[{"arg2", "\[Rule]", "val2"}], ",", "\[Ellipsis]"}], "}"}], ",",
       "\[IndentingNewLine]", 
      RowBox[{"(*", " ", 
       RowBox[{"Method", " ", 
        RowBox[{"body", "."}]}], " ", "*)"}], "\[IndentingNewLine]", 
      "\[Ellipsis]"}], "\[IndentingNewLine]", "]"}], Cell[TextData[{
     "Define a method with give ",
     StyleBox["name", "TI"],
     " and given ",
     StyleBox["body", "TI"],
     ".\nIn this form, the method will have default values for some of its \
argument, which will be used, if an argument will be omitted.\nThis format \
can be comined with auxiliary methods ",
     StyleBox["before", "TI"],
     " and ",
     StyleBox["after", "TI"],
     "."
    }], "TableText"]}
  }]], "2ColumnTableMod",
 GridBoxOptions->{
 GridBoxBackground->{
  "Columns" -> {{None}}, "ColumnsIndexed" -> {}, 
   "Rows" -> {None, None, None, None, {None}}, "RowsIndexed" -> {}},
 GridBoxDividers->{"Rows" -> {{True, True, True, True, True, True}}}},
 CellID->410394005,
 $CellContext`CellUUID -> "354F5EC4-E77E-4242-96BF-75125F617305"],

Cell["", "SectionFooterSpacer"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[{
 Cell[BoxData[
  InterpretationBox[
   StyleBox[
    GraphicsBox[{},
     BaselinePosition->Baseline,
     ImageSize->{6, 0}],
    CacheGraphics->False],
   Spacer[6]]]],
 "Examples"
}], "PrimaryExamplesSection",
 WholeCellGroupOpener->True,
 CellTags->"PrimaryExamplesSection",
 CellID->1195052388],

Cell[CellGroupData[{

Cell["Basic example", "ExampleSection", "ExampleSection",
 WholeCellGroupOpener->True,
 CellID->1978418372],

Cell[BoxData[
 RowBox[{"<<", "Objects`"}]], "Input",
 CellChangeTimes->{{3.703571888770885*^9, 3.703571890101639*^9}},
 CellLabel->"In[1]:=",
 CellID->1855307002],

Cell["Create a \"greeter\" class:", "ExampleText",
 CellID->53657308],

Cell[BoxData[
 RowBox[{
  RowBox[{"CNew", "[", 
   RowBox[{"Greeter", ",", 
    RowBox[{"{", "}"}], ",", "\[IndentingNewLine]", 
    RowBox[{"{", "\[IndentingNewLine]", 
     RowBox[{"$Method", "[", 
      RowBox[{"greet", ",", 
       RowBox[{"{", 
        RowBox[{"this", ",", "name"}], "}"}], ",", "\[IndentingNewLine]", 
       RowBox[{
        RowBox[{"Print", "[", 
         RowBox[{"\"\<Hello, \>\"", ",", "name", ",", "\"\<!\>\""}], "]"}], 
        ";"}]}], "\[IndentingNewLine]", "]"}], "\[IndentingNewLine]", "}"}]}],
    "\[IndentingNewLine]", "]"}], ";"}]], "Input",
 CellLabel->"In[2]:=",
 CellID->950116573],

Cell[TextData[{
 "Create an instance of ",
 Cell[BoxData["Greeter"], "InlineFormula",
  FormatType->"StandardForm"],
 ":"
}], "ExampleText",
 CellID->1666997269],

Cell[BoxData[
 RowBox[{
  RowBox[{"ONew", "[", 
   RowBox[{"greeter", ",", "Greeter"}], "]"}], ";"}]], "Input",
 CellLabel->"In[3]:=",
 CellID->487079380],

Cell[TextData[{
 "Call the method ",
 Cell[BoxData["greet"], "InlineFormula",
  FormatType->"StandardForm"],
 ":"
}], "ExampleText",
 CellID->812994254],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"greeter", "[", 
   RowBox[{"greet", ",", "\"\<John\>\""}], "]"}], ";"}]], "Input",
 CellLabel->"In[4]:=",
 CellID->643337105],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"Hello, \"\>", "\[InvisibleSpace]", "\<\"John\"\>", 
   "\[InvisibleSpace]", "\<\"!\"\>"}],
  SequenceForm["Hello, ", "John", "!"],
  Editable->False]], "Print",
 CellLabel->"During evaluation of In[4]:=",
 CellID->1938168860]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"greeter", "[", 
   RowBox[{"greet", ",", "\"\<Marie\>\""}], "]"}], ";"}]], "Input",
 CellLabel->"In[5]:=",
 CellID->322920761],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"Hello, \"\>", "\[InvisibleSpace]", "\<\"Marie\"\>", 
   "\[InvisibleSpace]", "\<\"!\"\>"}],
  SequenceForm["Hello, ", "Marie", "!"],
  Editable->False]], "Print",
 CellLabel->"During evaluation of In[5]:=",
 CellID->2117758762]
}, Open  ]],

Cell["Clean up:", "ExampleText",
 CellID->1815690007],

Cell[BoxData[
 RowBox[{"ODelete", "[", "greeter", "]"}]], "Input",
 CellChangeTimes->{{3.703572372217407*^9, 3.703572375189578*^9}},
 CellLabel->"In[6]:=",
 CellID->1614784492],

Cell[BoxData[
 RowBox[{"CDelete", "[", "Greeter", "]"}]], "Input",
 CellChangeTimes->{{3.70357236520051*^9, 3.7035723703026133`*^9}},
 CellLabel->"In[7]:=",
 CellID->45367038]
}, Open  ]],

Cell[CellGroupData[{

Cell["Default values", "ExampleSection", "ExampleSection",
 WholeCellGroupOpener->True,
 CellID->19911176],

Cell[BoxData[
 RowBox[{"<<", "Objects`"}]], "Input",
 CellChangeTimes->{{3.703571888770885*^9, 3.703571890101639*^9}},
 CellLabel->"In[1]:=",
 CellID->1940347290],

Cell["\<\
You can provide default values for some arguments of the method:\
\>", "ExampleText",
 CellID->261119511],

Cell[BoxData[
 RowBox[{
  RowBox[{"CNew", "[", 
   RowBox[{"Greeter", ",", 
    RowBox[{"{", "}"}], ",", "\[IndentingNewLine]", 
    RowBox[{"{", "\[IndentingNewLine]", 
     RowBox[{"$Method", "[", 
      RowBox[{"greet", ",", 
       RowBox[{"{", 
        RowBox[{"this", ",", 
         RowBox[{"name1", "\[Rule]", "\"\<John\>\""}], ",", 
         RowBox[{"name2", "\[Rule]", "\"\<Mike\>\""}]}], "}"}], ",", 
       "\[IndentingNewLine]", 
       RowBox[{"Print", "[", 
        RowBox[{
        "\"\<Hi, \>\"", ",", "name1", ",", "\"\< and \>\"", ",", "name2", 
         ",", "\"\<!\>\""}], "]"}]}], "\[IndentingNewLine]", "]"}], 
     "\[IndentingNewLine]", "}"}]}], "\[IndentingNewLine]", "]"}], 
  ";"}]], "Input",
 CellLabel->"In[2]:=",
 CellID->580176418],

Cell[TextData[{
 "Create an instance of ",
 Cell[BoxData["Greeter"], "InlineFormula"],
 " with ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["ONew"]],"paclet:Objects/ref/ONew"},
   "RefLink",
   BaseStyle->"InlineFormula"]], "InlineFormula"],
 ":"
}], "ExampleText",
 CellID->1157096663],

Cell[BoxData[
 RowBox[{
  RowBox[{"ONew", "[", 
   RowBox[{"greeter", ",", "Greeter"}], "]"}], ";"}]], "Input",
 CellLabel->"In[3]:=",
 CellID->1485053130],

Cell[TextData[{
 "Call its method ",
 Cell[BoxData["greet"], "InlineFormula",
  FormatType->"StandardForm"],
 ":"
}], "ExampleText",
 CellID->1154245812],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"greeter", "[", "greet", "]"}]], "Input",
 CellLabel->"In[4]:=",
 CellID->1493080666],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"Hi, \"\>", "\[InvisibleSpace]", "\<\"John\"\>", 
   "\[InvisibleSpace]", "\<\" and \"\>", "\[InvisibleSpace]", "\<\"Mike\"\>", 
   "\[InvisibleSpace]", "\<\"!\"\>"}],
  SequenceForm["Hi, ", "John", " and ", "Mike", "!"],
  Editable->False]], "Print",
 CellLabel->"During evaluation of In[4]:=",
 CellID->707489709]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"greeter", "[", 
  RowBox[{"greet", ",", "\"\<Marie\>\""}], "]"}]], "Input",
 CellLabel->"In[5]:=",
 CellID->1035005867],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"Hi, \"\>", "\[InvisibleSpace]", "\<\"Marie\"\>", 
   "\[InvisibleSpace]", "\<\" and \"\>", "\[InvisibleSpace]", "\<\"Mike\"\>", 
   "\[InvisibleSpace]", "\<\"!\"\>"}],
  SequenceForm["Hi, ", "Marie", " and ", "Mike", "!"],
  Editable->False]], "Print",
 CellLabel->"During evaluation of In[5]:=",
 CellID->1819931356]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"greeter", "[", 
  RowBox[{"greet", ",", "\"\<Marie\>\"", ",", "\"\<William\>\""}], 
  "]"}]], "Input",
 CellLabel->"In[6]:=",
 CellID->1832508825],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"Hi, \"\>", "\[InvisibleSpace]", "\<\"Marie\"\>", 
   "\[InvisibleSpace]", "\<\" and \"\>", 
   "\[InvisibleSpace]", "\<\"William\"\>", "\[InvisibleSpace]", "\<\"!\"\>"}],
  
  SequenceForm["Hi, ", "Marie", " and ", "William", "!"],
  Editable->False]], "Print",
 CellLabel->"During evaluation of In[6]:=",
 CellID->467445534]
}, Open  ]],

Cell[BoxData[
 RowBox[{"ODelete", "[", "greeter", "]"}]], "Input",
 CellLabel->"In[7]:=",
 CellID->1714855831],

Cell[BoxData[
 RowBox[{"CDelete", "[", "Greeter", "]"}]], "Input",
 CellChangeTimes->{{3.704768440899979*^9, 3.7047684434228573`*^9}},
 CellLabel->"In[8]:=",
 CellID->1542651858]
}, Closed]],

Cell["", "SectionFooterSpacer",
 CellID->2115137253]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[{
 Cell[BoxData[
  InterpretationBox[
   StyleBox[
    GraphicsBox[{},
     BaselinePosition->Baseline,
     ImageSize->{6, 0}],
    CacheGraphics->False],
   Spacer[6]]]],
 "See Also"
}], "SeeAlsoSection",
 WholeCellGroupOpener->True,
 CellID->20994],

Cell[TextData[{
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["CNew"]],"paclet:Objects/ref/CNew"},
   "RefLink",
   BaseStyle->{"InlineFormula", FontFamily -> "Verdana"}]], "InlineFormula"],
 "\[NonBreakingSpace]",
 StyleBox["\[MediumSpace]\[FilledVerySmallSquare]\[MediumSpace]", 
  "InlineSeparator"],
 " ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["CDelete"]],"paclet:Objects/ref/CDelete"},
   "RefLink",
   BaseStyle->{"InlineFormula", FontFamily -> "Verdana"}]], "InlineFormula"],
 "\[NonBreakingSpace]",
 StyleBox["\[MediumSpace]\[FilledVerySmallSquare]\[MediumSpace]", 
  "InlineSeparator"],
 " ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["$Constructor"]],"paclet:Objects/ref/$Constructor"},
   "RefLink",
   BaseStyle->{"InlineFormula", FontFamily -> "Verdana"}]], "InlineFormula"],
 "\[NonBreakingSpace]",
 StyleBox["\[MediumSpace]\[FilledVerySmallSquare]\[MediumSpace]", 
  "InlineSeparator"],
 " ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["$Destructor"]],"paclet:Objects/ref/$Destructor"},
   "RefLink",
   BaseStyle->{"InlineFormula", FontFamily -> "Verdana"}]], "InlineFormula"]
}], "SeeAlso",
 CellID->18286]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[{
 Cell[BoxData[
  InterpretationBox[
   StyleBox[
    GraphicsBox[{},
     BaselinePosition->Baseline,
     ImageSize->{6, 0}],
    CacheGraphics->False],
   Spacer[6]]]],
 "Related Guides"
}], "MoreAboutSection",
 WholeCellGroupOpener->True,
 CellID->1973046713],

Cell["", "SectionHeaderSpacer"],

Cell[BoxData[
 TemplateBox[{Cell[
    TextData["Objects Package"]],"paclet:Objects/guide/ObjectsPackage"},
  "RefLink",
  BaseStyle->"MoreAbout"]], "MoreAbout",
 CellID->1788271703]
}, Open  ]],

Cell[" ", "FooterCell"]
},
Saveable->False,
ScreenStyleEnvironment->"Working",
WindowSize->{1918, 1005},
WindowMargins->{{-9, Automatic}, {Automatic, -8}},
WindowTitle->"$Method",
TaggingRules->{
 "ModificationHighlight" -> False, 
  "Metadata" -> {
   "context" -> "Objects`", "keywords" -> {}, "index" -> True, "label" -> 
    "Objects Package Symbol", "language" -> "en", "paclet" -> 
    "Objects Package", "status" -> "", "summary" -> "", "synonyms" -> {}, 
    "title" -> "$Method", "type" -> "Symbol", "uri" -> "Objects/ref/$Method"},
   "SearchTextTranslated" -> "", "LinkTrails" -> ""},
FrontEndVersion->"10.4 for Linux x86 (64-bit) (April 11, 2016)",
StyleDefinitions->FrontEnd`FileName[{"Wolfram"}, "Reference.nb", 
  CharacterEncoding -> "UTF-8"]
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{
 "PrimaryExamplesSection"->{
  Cell[7500, 246, 315, 13, 39, "PrimaryExamplesSection",
   CellTags->"PrimaryExamplesSection",
   CellID->1195052388]}
 }
*)
(*CellTagsIndex
CellTagsIndex->{
 {"PrimaryExamplesSection", 17349, 615}
 }
*)
(*NotebookFileOutline
Notebook[{
Cell[579, 21, 112, 3, 43, "AnchorBarGrid",
 CellID->1],
Cell[694, 26, 239, 10, 61, "ObjectNameGrid"],
Cell[CellGroupData[{
Cell[958, 40, 486, 14, 96, "Usage",
 CellID->17713],
Cell[CellGroupData[{
Cell[1469, 58, 737, 24, 40, "NotesSection",
 CellGroupingRules->{"SectionGrouping", 50},
 CellID->75457274],
Cell[2209, 84, 31, 0, 14, "SectionHeaderSpacer"],
Cell[2243, 86, 211, 7, 36, "Notes",
 CellID->718278630],
Cell[2457, 95, 277, 9, 39, "Notes",
 CellID->622894829],
Cell[2737, 106, 4680, 132, 363, "2ColumnTableMod",
 CellID->410394005],
Cell[7420, 240, 31, 0, 29, "SectionFooterSpacer"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[7500, 246, 315, 13, 39, "PrimaryExamplesSection",
 CellTags->"PrimaryExamplesSection",
 CellID->1195052388],
Cell[CellGroupData[{
Cell[7840, 263, 107, 2, 33, "ExampleSection",
 CellID->1978418372],
Cell[7950, 267, 162, 4, 33, "Input",
 CellID->1855307002],
Cell[8115, 273, 69, 1, 40, "ExampleText",
 CellID->53657308],
Cell[8187, 276, 621, 16, 172, "Input",
 CellID->950116573],
Cell[8811, 294, 161, 6, 41, "ExampleText",
 CellID->1666997269],
Cell[8975, 302, 154, 5, 33, "Input",
 CellID->487079380],
Cell[9132, 309, 152, 6, 41, "ExampleText",
 CellID->812994254],
Cell[CellGroupData[{
Cell[9309, 319, 160, 5, 33, "Input",
 CellID->643337105],
Cell[9472, 326, 274, 7, 29, "Print",
 CellID->1938168860]
}, Open  ]],
Cell[CellGroupData[{
Cell[9783, 338, 161, 5, 33, "Input",
 CellID->322920761],
Cell[9947, 345, 276, 7, 29, "Print",
 CellID->2117758762]
}, Open  ]],
Cell[10238, 355, 53, 1, 40, "ExampleText",
 CellID->1815690007],
Cell[10294, 358, 176, 4, 33, "Input",
 CellID->1614784492],
Cell[10473, 364, 175, 4, 33, "Input",
 CellID->45367038]
}, Open  ]],
Cell[CellGroupData[{
Cell[10685, 373, 106, 2, 33, "ExampleSection",
 CellID->19911176],
Cell[10794, 377, 162, 4, 33, "Input",
 CellID->1940347290],
Cell[10959, 383, 115, 3, 40, "ExampleText",
 CellID->261119511],
Cell[11077, 388, 762, 20, 172, "Input",
 CellID->580176418],
Cell[11842, 410, 294, 11, 43, "ExampleText",
 CellID->1157096663],
Cell[12139, 423, 155, 5, 33, "Input",
 CellID->1485053130],
Cell[12297, 430, 153, 6, 41, "ExampleText",
 CellID->1154245812],
Cell[CellGroupData[{
Cell[12475, 440, 108, 3, 33, "Input",
 CellID->1493080666],
Cell[12586, 445, 363, 8, 29, "Print",
 CellID->707489709]
}, Open  ]],
Cell[CellGroupData[{
Cell[12986, 458, 143, 4, 33, "Input",
 CellID->1035005867],
Cell[13132, 464, 366, 8, 29, "Print",
 CellID->1819931356]
}, Open  ]],
Cell[CellGroupData[{
Cell[13535, 477, 170, 5, 33, "Input",
 CellID->1832508825],
Cell[13708, 484, 374, 9, 29, "Print",
 CellID->467445534]
}, Open  ]],
Cell[14097, 496, 110, 3, 33, "Input",
 CellID->1714855831],
Cell[14210, 501, 178, 4, 33, "Input",
 CellID->1542651858]
}, Closed]],
Cell[14403, 508, 52, 1, 29, "SectionFooterSpacer",
 CellID->2115137253]
}, Open  ]],
Cell[CellGroupData[{
Cell[14492, 514, 265, 12, 39, "SeeAlsoSection",
 CellID->20994],
Cell[14760, 528, 1154, 34, 67, "SeeAlso",
 CellID->18286]
}, Open  ]],
Cell[CellGroupData[{
Cell[15951, 567, 278, 12, 39, "MoreAboutSection",
 CellID->1973046713],
Cell[16232, 581, 31, 0, 14, "SectionHeaderSpacer"],
Cell[16266, 583, 181, 5, 26, "MoreAbout",
 CellID->1788271703]
}, Open  ]],
Cell[16462, 591, 23, 0, 47, "FooterCell"]
}
]
*)


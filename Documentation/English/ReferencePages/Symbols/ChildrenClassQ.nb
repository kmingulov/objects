(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 10.4' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     10678,        417]
NotebookOptionsPosition[      7789,        320]
NotebookOutlinePosition[      8903,        355]
CellTagsIndexPosition[      8819,        350]
WindowTitle->ChildrenClassQ
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[GridBox[{
   {Cell["OBJECTS PACKAGE SYMBOL", "PacletNameCell"]}
  }]], "AnchorBarGrid",
 CellID->1],

Cell[TextData[{
 Cell["ChildrenClassQ", "ObjectName"],
 Cell[BoxData[
  InterpretationBox[
   StyleBox[
    GraphicsBox[{},
     BaselinePosition->Baseline,
     ImageSize->{8, 0}],
    CacheGraphics->False],
   Spacer[8]]]]
}], "ObjectNameGrid"],

Cell[CellGroupData[{

Cell[BoxData[GridBox[{
   {"", Cell[TextData[{
     Cell[BoxData[
      RowBox[{
       TemplateBox[{Cell[
          TextData["ChildrenClassQ"]],"paclet:Objects/ref/ChildrenClassQ"},
        "RefLink",
        BaseStyle->"InlineFormula"], "[", 
       RowBox[{
        StyleBox["child", "TI"], ",", 
        StyleBox["parent", "TI"]}], "]"}]], "InlineFormula"],
     " \[LineSeparator]checks whether ",
     StyleBox["child", "TI"],
     " inherits ",
     StyleBox["parent", "TI"],
     "."
    }]]}
  }]], "Usage",
 GridBoxOptions->{
 GridBoxBackground->{
  "Columns" -> {{None}}, "ColumnsIndexed" -> {}, 
   "Rows" -> {None, None, {None}}, "RowsIndexed" -> {}}},
 CellID->17713],

Cell[CellGroupData[{

Cell[TextData[Cell[BoxData[
 ButtonBox[Cell[TextData[{
   Cell[BoxData[
    InterpretationBox[
     StyleBox[
      GraphicsBox[{},
       BaselinePosition->Baseline,
       ImageSize->{6, 0}],
      CacheGraphics->False],
     Spacer[6]]]],
   "Details"
  }], "NotesFrameText"],
  Appearance->{Automatic, None},
  BaseStyle->None,
  ButtonFunction:>(FrontEndExecute[{
     FrontEnd`SelectionMove[
      FrontEnd`SelectedNotebook[], All, ButtonCell], 
     FrontEndToken["OpenCloseGroup"], 
     FrontEnd`SelectionMove[
      FrontEnd`SelectedNotebook[], After, CellContents]}]& ),
  Evaluator->None,
  Method->"Preemptive"]]]], "NotesSection",
 WholeCellGroupOpener->True,
 CellGroupingRules->{"SectionGrouping", 50},
 CellID->75457274],

Cell["", "SectionHeaderSpacer"],

Cell[TextData[{
 "To use this function, you first need to load the ",
 ButtonBox["Objects Package",
  BaseStyle->"Link",
  ButtonData->"paclet:Objects/guide/ObjectsPackage"],
 "."
}], "Notes",
 CellID->718278630],

Cell[TextData[{
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["ChildrenClassQ"]],"paclet:Objects/ref/ChildrenClassQ"},
   "RefLink",
   BaseStyle->"InlineFormula"]], "InlineFormula"],
 " returns ",
 StyleBox["True", "InlineFormula"],
 ", even if ",
 StyleBox["child", "TI"],
 " does not directly inherit ",
 StyleBox["parent", "TI"],
 "."
}], "Notes",
 CellID->136833138],

Cell["", "SectionFooterSpacer"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[{
 Cell[BoxData[
  InterpretationBox[
   StyleBox[
    GraphicsBox[{},
     BaselinePosition->Baseline,
     ImageSize->{6, 0}],
    CacheGraphics->False],
   Spacer[6]]]],
 "Examples"
}], "PrimaryExamplesSection",
 WholeCellGroupOpener->True,
 CellTags->"PrimaryExamplesSection",
 CellID->1195052388],

Cell[CellGroupData[{

Cell["Basic example", "ExampleSection", "ExampleSection",
 WholeCellGroupOpener->True,
 CellID->1978418372],

Cell[BoxData[
 RowBox[{"<<", "Objects`"}]], "Input",
 CellChangeTimes->{{3.703571888770885*^9, 3.703571890101639*^9}},
 CellLabel->"In[1]:=",
 CellID->2007368699],

Cell[TextData[{
 "Create several simple classes with ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["CNew"]],"paclet:Objects/ref/CNew"},
   "RefLink",
   BaseStyle->"InlineFormula"]], "InlineFormula"],
 ":"
}], "ExampleText",
 CellID->477905686],

Cell[BoxData[
 RowBox[{
  RowBox[{"CNew", "[", 
   RowBox[{"ClassA", ",", 
    RowBox[{"{", "}"}], ",", 
    RowBox[{"{", "}"}]}], "]"}], ";"}]], "Input",
 CellLabel->"In[2]:=",
 CellID->340848625],

Cell[BoxData[
 RowBox[{
  RowBox[{"CNew", "[", 
   RowBox[{"ClassB", ",", 
    RowBox[{"{", "}"}], ",", 
    RowBox[{"{", "}"}]}], "]"}], ";"}]], "Input",
 CellLabel->"In[3]:=",
 CellID->1835678537],

Cell[BoxData[
 RowBox[{
  RowBox[{"CNew", "[", 
   RowBox[{
    RowBox[{"ClassC", "\[Rule]", "ClassA"}], ",", 
    RowBox[{"{", "}"}], ",", 
    RowBox[{"{", "}"}]}], "]"}], ";"}]], "Input",
 CellLabel->"In[4]:=",
 CellID->1888323599],

Cell[BoxData[
 RowBox[{
  RowBox[{"CNew", "[", 
   RowBox[{
    RowBox[{"ClassD", "\[Rule]", "ClassC"}], ",", 
    RowBox[{"{", "}"}], ",", 
    RowBox[{"{", "}"}]}], "]"}], ";"}]], "Input",
 CellLabel->"In[5]:=",
 CellID->1728611768],

Cell["Perform several checks:", "ExampleText",
 CellID->1200443100],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"ChildrenClassQ", "[", 
    RowBox[{"#", ",", "ClassA"}], "]"}], "&"}], "/@", 
  RowBox[{"{", 
   RowBox[{"ClassA", ",", "ClassB", ",", "ClassC", ",", "ClassD"}], 
   "}"}]}]], "Input",
 CellChangeTimes->{{3.703765104192848*^9, 3.703765120989238*^9}},
 CellLabel->"In[6]:=",
 CellID->1787425661],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"False", ",", "False", ",", "True", ",", "True"}], "}"}]], "Output",\

 CellLabel->"Out[6]=",
 CellID->1971667226]
}, Open  ]],

Cell[BoxData[
 RowBox[{"CDelete", "[", 
  RowBox[{"ClassA", ",", "ClassB", ",", "ClassC", ",", "ClassD"}], 
  "]"}]], "Input",
 CellChangeTimes->{{3.703765190603162*^9, 3.70376519236164*^9}},
 CellLabel->"In[7]:=",
 CellID->1247145880]
}, Open  ]],

Cell["", "SectionFooterSpacer",
 CellID->2115137253]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[{
 Cell[BoxData[
  InterpretationBox[
   StyleBox[
    GraphicsBox[{},
     BaselinePosition->Baseline,
     ImageSize->{6, 0}],
    CacheGraphics->False],
   Spacer[6]]]],
 "See Also"
}], "SeeAlsoSection",
 WholeCellGroupOpener->True,
 CellID->20994],

Cell[TextData[{
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["CNew"]],"paclet:Objects/ref/CNew"},
   "RefLink",
   BaseStyle->{"InlineFormula", FontFamily -> "Verdana"}]], "InlineFormula"],
 "\[NonBreakingSpace]",
 StyleBox["\[MediumSpace]\[FilledVerySmallSquare]\[MediumSpace]", 
  "InlineSeparator"],
 " ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["CDelete"]],"paclet:Objects/ref/CDelete"},
   "RefLink",
   BaseStyle->{"InlineFormula", FontFamily -> "Verdana"}]], "InlineFormula"],
 "\[NonBreakingSpace]",
 StyleBox["\[MediumSpace]\[FilledVerySmallSquare]\[MediumSpace]", 
  "InlineSeparator"],
 " ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["ClassQ"]],"paclet:Objects/ref/ClassQ"},
   "RefLink",
   BaseStyle->{"InlineFormula", FontFamily -> "Verdana"}]], "InlineFormula"],
 "\[NonBreakingSpace]",
 StyleBox["\[MediumSpace]\[FilledVerySmallSquare]\[MediumSpace]", 
  "InlineSeparator"],
 " ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["ObjectQ"]],"paclet:Objects/ref/ObjectQ"},
   "RefLink",
   BaseStyle->{"InlineFormula", FontFamily -> "Verdana"}]], "InlineFormula"]
}], "SeeAlso",
 CellID->18286]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[{
 Cell[BoxData[
  InterpretationBox[
   StyleBox[
    GraphicsBox[{},
     BaselinePosition->Baseline,
     ImageSize->{6, 0}],
    CacheGraphics->False],
   Spacer[6]]]],
 "Related Guides"
}], "MoreAboutSection",
 WholeCellGroupOpener->True,
 CellID->1973046713],

Cell["", "SectionHeaderSpacer"],

Cell[BoxData[
 TemplateBox[{Cell[
    TextData["Objects Package"]],"paclet:Objects/guide/ObjectsPackage"},
  "RefLink",
  BaseStyle->"MoreAbout"]], "MoreAbout",
 CellID->1788271703]
}, Open  ]],

Cell[" ", "FooterCell"]
},
Saveable->False,
ScreenStyleEnvironment->"Working",
WindowSize->{1918, 1005},
WindowMargins->{{-9, Automatic}, {Automatic, -8}},
WindowTitle->"ChildrenClassQ",
TaggingRules->{
 "ModificationHighlight" -> False, 
  "Metadata" -> {
   "context" -> "Objects`", "keywords" -> {}, "index" -> True, "label" -> 
    "Objects Package Symbol", "language" -> "en", "paclet" -> 
    "Objects Package", "status" -> "", "summary" -> "", "synonyms" -> {}, 
    "title" -> "ChildrenClassQ", "type" -> "Symbol", "uri" -> 
    "Objects/ref/ChildrenClassQ"}, "SearchTextTranslated" -> "", "LinkTrails" -> 
  ""},
FrontEndVersion->"10.4 for Linux x86 (64-bit) (April 11, 2016)",
StyleDefinitions->FrontEnd`FileName[{"Wolfram"}, "Reference.nb", 
  CharacterEncoding -> "UTF-8"]
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{
 "PrimaryExamplesSection"->{
  Cell[3125, 125, 315, 13, 39, "PrimaryExamplesSection",
   CellTags->"PrimaryExamplesSection",
   CellID->1195052388]}
 }
*)
(*CellTagsIndex
CellTagsIndex->{
 {"PrimaryExamplesSection", 8675, 343}
 }
*)
(*NotebookFileOutline
Notebook[{
Cell[586, 21, 112, 3, 43, "AnchorBarGrid",
 CellID->1],
Cell[701, 26, 246, 10, 61, "ObjectNameGrid"],
Cell[CellGroupData[{
Cell[972, 40, 681, 22, 97, "Usage",
 CellID->17713],
Cell[CellGroupData[{
Cell[1678, 66, 737, 24, 40, "NotesSection",
 CellGroupingRules->{"SectionGrouping", 50},
 CellID->75457274],
Cell[2418, 92, 31, 0, 14, "SectionHeaderSpacer"],
Cell[2452, 94, 212, 7, 36, "Notes",
 CellID->718278630],
Cell[2667, 103, 375, 14, 39, "Notes",
 CellID->136833138],
Cell[3045, 119, 31, 0, 29, "SectionFooterSpacer"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[3125, 125, 315, 13, 39, "PrimaryExamplesSection",
 CellTags->"PrimaryExamplesSection",
 CellID->1195052388],
Cell[CellGroupData[{
Cell[3465, 142, 107, 2, 33, "ExampleSection",
 CellID->1978418372],
Cell[3575, 146, 162, 4, 33, "Input",
 CellID->2007368699],
Cell[3740, 152, 251, 9, 43, "ExampleText",
 CellID->477905686],
Cell[3994, 163, 197, 7, 33, "Input",
 CellID->340848625],
Cell[4194, 172, 198, 7, 33, "Input",
 CellID->1835678537],
Cell[4395, 181, 234, 8, 33, "Input",
 CellID->1888323599],
Cell[4632, 191, 234, 8, 33, "Input",
 CellID->1728611768],
Cell[4869, 201, 67, 1, 40, "ExampleText",
 CellID->1200443100],
Cell[CellGroupData[{
Cell[4961, 206, 341, 10, 33, "Input",
 CellID->1787425661],
Cell[5305, 218, 153, 5, 42, "Output",
 CellID->1971667226]
}, Open  ]],
Cell[5473, 226, 235, 6, 33, "Input",
 CellID->1247145880]
}, Open  ]],
Cell[5723, 235, 52, 1, 29, "SectionFooterSpacer",
 CellID->2115137253]
}, Open  ]],
Cell[CellGroupData[{
Cell[5812, 241, 265, 12, 39, "SeeAlsoSection",
 CellID->20994],
Cell[6080, 255, 1134, 34, 67, "SeeAlso",
 CellID->18286]
}, Open  ]],
Cell[CellGroupData[{
Cell[7251, 294, 278, 12, 39, "MoreAboutSection",
 CellID->1973046713],
Cell[7532, 308, 31, 0, 14, "SectionHeaderSpacer"],
Cell[7566, 310, 181, 5, 26, "MoreAbout",
 CellID->1788271703]
}, Open  ]],
Cell[7762, 318, 23, 0, 47, "FooterCell"]
}
]
*)


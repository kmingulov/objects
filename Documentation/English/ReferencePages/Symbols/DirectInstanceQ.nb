(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 10.4' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     11633,        459]
NotebookOptionsPosition[      8469,        352]
NotebookOutlinePosition[      9586,        387]
CellTagsIndexPosition[      9502,        382]
WindowTitle->DirectInstanceQ
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[GridBox[{
   {Cell["OBJECTS PACKAGE SYMBOL", "PacletNameCell"]}
  }]], "AnchorBarGrid",
 CellID->1],

Cell[TextData[{
 Cell["DirectInstanceQ", "ObjectName"],
 Cell[BoxData[
  InterpretationBox[
   StyleBox[
    GraphicsBox[{},
     BaselinePosition->Baseline,
     ImageSize->{8, 0}],
    CacheGraphics->False],
   Spacer[8]]]]
}], "ObjectNameGrid"],

Cell[CellGroupData[{

Cell[BoxData[GridBox[{
   {"", Cell[TextData[{
     Cell[BoxData[
      RowBox[{
       TemplateBox[{Cell[
          TextData["DirectInstanceQ"]],"paclet:Objects/ref/DirectInstanceQ"},
        "RefLink",
        BaseStyle->"InlineFormula"], "[", 
       RowBox[{
        StyleBox["obj", "TI"], ",", 
        StyleBox["class", "TI"]}], "]"}]], "InlineFormula"],
     " \[LineSeparator]checks whether ",
     StyleBox["obj", "TI"],
     " is a direct instance of ",
     StyleBox["class", "TI"],
     "."
    }]]}
  }]], "Usage",
 GridBoxOptions->{
 GridBoxBackground->{
  "Columns" -> {{None}}, "ColumnsIndexed" -> {}, 
   "Rows" -> {None, None, {None}}, "RowsIndexed" -> {}}},
 CellID->17713],

Cell[CellGroupData[{

Cell[TextData[Cell[BoxData[
 ButtonBox[Cell[TextData[{
   Cell[BoxData[
    InterpretationBox[
     StyleBox[
      GraphicsBox[{},
       BaselinePosition->Baseline,
       ImageSize->{6, 0}],
      CacheGraphics->False],
     Spacer[6]]]],
   "Details"
  }], "NotesFrameText"],
  Appearance->{Automatic, None},
  BaseStyle->None,
  ButtonFunction:>(FrontEndExecute[{
     FrontEnd`SelectionMove[
      FrontEnd`SelectedNotebook[], All, ButtonCell], 
     FrontEndToken["OpenCloseGroup"], 
     FrontEnd`SelectionMove[
      FrontEnd`SelectedNotebook[], After, CellContents]}]& ),
  Evaluator->None,
  Method->"Preemptive"]]]], "NotesSection",
 WholeCellGroupOpener->True,
 CellGroupingRules->{"SectionGrouping", 50},
 CellID->75457274],

Cell["", "SectionHeaderSpacer"],

Cell[TextData[{
 "To use this function, you first need to load the ",
 ButtonBox["Objects Package",
  BaseStyle->"Link",
  ButtonData->"paclet:Objects/guide/ObjectsPackage"],
 "."
}], "Notes",
 CellID->718278630],

Cell[TextData[{
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["DirectInstanceQ"]],"paclet:Objects/ref/DirectInstanceQ"},
   "RefLink",
   BaseStyle->"InlineFormula"]], "InlineFormula"],
 " returns ",
 StyleBox["True", "InlineFormula"],
 ", only if ",
 StyleBox["obj", "TI"],
 " is a direct instance of ",
 StyleBox["parent", "TI"],
 "."
}], "Notes",
 CellID->136833138],

Cell[TextData[{
 "In order to check, whether ",
 StyleBox["obj", "TI"],
 " is an instance of any class, which inherits ",
 StyleBox["class", "TI"],
 ", use ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["InstanceQ"]],"paclet:Objects/ref/InstanceQ"},
   "RefLink",
   BaseStyle->"InlineFormula"]], "InlineFormula"],
 " instead."
}], "Notes",
 CellID->994190625],

Cell["", "SectionFooterSpacer"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[{
 Cell[BoxData[
  InterpretationBox[
   StyleBox[
    GraphicsBox[{},
     BaselinePosition->Baseline,
     ImageSize->{6, 0}],
    CacheGraphics->False],
   Spacer[6]]]],
 "Examples"
}], "PrimaryExamplesSection",
 WholeCellGroupOpener->True,
 CellTags->"PrimaryExamplesSection",
 CellID->1195052388],

Cell[CellGroupData[{

Cell["Basic example", "ExampleSection", "ExampleSection",
 WholeCellGroupOpener->True,
 CellID->1978418372],

Cell[BoxData[
 RowBox[{"<<", "Objects`"}]], "Input",
 CellChangeTimes->{{3.703571888770885*^9, 3.703571890101639*^9}},
 CellLabel->"In[1]:=",
 CellID->2007368699],

Cell[TextData[{
 "Create several simple classes with ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["CNew"]],"paclet:Objects/ref/CNew"},
   "RefLink",
   BaseStyle->"InlineFormula"]], "InlineFormula"],
 ":"
}], "ExampleText",
 CellID->477905686],

Cell[BoxData[
 RowBox[{
  RowBox[{"CNew", "[", 
   RowBox[{"ClassA", ",", 
    RowBox[{"{", "}"}], ",", 
    RowBox[{"{", "}"}]}], "]"}], ";"}]], "Input",
 CellLabel->"In[2]:=",
 CellID->340848625],

Cell[BoxData[
 RowBox[{
  RowBox[{"CNew", "[", 
   RowBox[{"ClassB", ",", 
    RowBox[{"{", "}"}], ",", 
    RowBox[{"{", "}"}]}], "]"}], ";"}]], "Input",
 CellLabel->"In[3]:=",
 CellID->1835678537],

Cell[BoxData[
 RowBox[{
  RowBox[{"CNew", "[", 
   RowBox[{
    RowBox[{"ClassC", "\[Rule]", "ClassA"}], ",", 
    RowBox[{"{", "}"}], ",", 
    RowBox[{"{", "}"}]}], "]"}], ";"}]], "Input",
 CellLabel->"In[4]:=",
 CellID->1888323599],

Cell[TextData[{
 "Create an instance of ",
 StyleBox["ClassC", "InlineFormula"],
 ":"
}], "ExampleText",
 CellID->1200443100],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"ONew", "[", 
  RowBox[{"obj", ",", "ClassC"}], "]"}]], "Input",
 CellChangeTimes->{{3.7037655926849337`*^9, 3.703765600606721*^9}},
 CellLabel->"In[5]:=",
 CellID->832821921],

Cell[BoxData["obj"], "Output",
 CellLabel->"Out[5]=",
 CellID->2097904239]
}, Open  ]],

Cell[TextData[{
 "Perform checks on ",
 StyleBox["obj", "InlineFormula"],
 ":"
}], "ExampleText",
 CellID->852042784],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"DirectInstanceQ", "[", 
    RowBox[{"obj", ",", "#"}], "]"}], "&"}], "/@", 
  RowBox[{"{", 
   RowBox[{"ClassA", ",", "ClassB", ",", "ClassC"}], "}"}]}]], "Input",
 CellChangeTimes->{{3.703765692007699*^9, 3.703765703262608*^9}, {
  3.7037659488514757`*^9, 3.7037659494961653`*^9}},
 CellLabel->"In[6]:=",
 CellID->692020249],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"False", ",", "False", ",", "True"}], "}"}]], "Output",
 CellLabel->"Out[6]=",
 CellID->1260285186]
}, Open  ]],

Cell[BoxData[
 RowBox[{"ODelete", "[", "obj", "]"}]], "Input",
 CellLabel->"In[7]:=",
 CellID->1247145880],

Cell[BoxData[
 RowBox[{"CDelete", "[", 
  RowBox[{"ClassA", ",", "ClassB", ",", "ClassC"}], "]"}]], "Input",
 CellLabel->"In[8]:=",
 CellID->173805481]
}, Open  ]],

Cell["", "SectionFooterSpacer",
 CellID->2115137253]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[{
 Cell[BoxData[
  InterpretationBox[
   StyleBox[
    GraphicsBox[{},
     BaselinePosition->Baseline,
     ImageSize->{6, 0}],
    CacheGraphics->False],
   Spacer[6]]]],
 "See Also"
}], "SeeAlsoSection",
 WholeCellGroupOpener->True,
 CellID->20994],

Cell[TextData[{
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["ONew"]],"paclet:Objects/ref/ONew"},
   "RefLink",
   BaseStyle->{"InlineFormula", FontFamily -> "Verdana"}]], "InlineFormula"],
 "\[NonBreakingSpace]",
 StyleBox["\[MediumSpace]\[FilledVerySmallSquare]\[MediumSpace]", 
  "InlineSeparator"],
 " ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["ODelete"]],"paclet:Objects/ref/ODelete"},
   "RefLink",
   BaseStyle->{"InlineFormula", FontFamily -> "Verdana"}]], "InlineFormula"],
 "\[NonBreakingSpace]",
 StyleBox["\[MediumSpace]\[FilledVerySmallSquare]\[MediumSpace]", 
  "InlineSeparator"],
 " ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["ObjectQ"]],"paclet:Objects/ref/ObjectQ"},
   "RefLink",
   BaseStyle->{"InlineFormula", FontFamily -> "Verdana"}]], "InlineFormula"],
 "\[NonBreakingSpace]",
 StyleBox["\[MediumSpace]\[FilledVerySmallSquare]\[MediumSpace]", 
  "InlineSeparator"],
 " ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["InstanceQ"]],"paclet:Objects/ref/InstanceQ"},
   "RefLink",
   BaseStyle->{"InlineFormula", FontFamily -> "Verdana"}]], "InlineFormula"]
}], "SeeAlso",
 CellID->18286]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[{
 Cell[BoxData[
  InterpretationBox[
   StyleBox[
    GraphicsBox[{},
     BaselinePosition->Baseline,
     ImageSize->{6, 0}],
    CacheGraphics->False],
   Spacer[6]]]],
 "Related Guides"
}], "MoreAboutSection",
 WholeCellGroupOpener->True,
 CellID->1973046713],

Cell["", "SectionHeaderSpacer"],

Cell[BoxData[
 TemplateBox[{Cell[
    TextData["Objects Package"]],"paclet:Objects/guide/ObjectsPackage"},
  "RefLink",
  BaseStyle->"MoreAbout"]], "MoreAbout",
 CellID->1788271703]
}, Open  ]],

Cell[" ", "FooterCell"]
},
Saveable->False,
ScreenStyleEnvironment->"Working",
WindowSize->{1918, 1005},
WindowMargins->{{-9, Automatic}, {Automatic, -8}},
WindowTitle->"DirectInstanceQ",
TaggingRules->{
 "ModificationHighlight" -> False, 
  "Metadata" -> {
   "context" -> "Objects`", "keywords" -> {}, "index" -> True, "label" -> 
    "Objects Package Symbol", "language" -> "en", "paclet" -> 
    "Objects Package", "status" -> "", "summary" -> "", "synonyms" -> {}, 
    "title" -> "DirectInstanceQ", "type" -> "Symbol", "uri" -> 
    "Objects/ref/DirectInstanceQ"}, "SearchTextTranslated" -> "", 
  "LinkTrails" -> ""},
FrontEndVersion->"10.4 for Linux x86 (64-bit) (April 11, 2016)",
StyleDefinitions->FrontEnd`FileName[{"Wolfram"}, "Reference.nb", 
  CharacterEncoding -> "UTF-8"]
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{
 "PrimaryExamplesSection"->{
  Cell[3505, 140, 315, 13, 39, "PrimaryExamplesSection",
   CellTags->"PrimaryExamplesSection",
   CellID->1195052388]}
 }
*)
(*CellTagsIndex
CellTagsIndex->{
 {"PrimaryExamplesSection", 9358, 375}
 }
*)
(*NotebookFileOutline
Notebook[{
Cell[587, 21, 112, 3, 43, "AnchorBarGrid",
 CellID->1],
Cell[702, 26, 247, 10, 61, "ObjectNameGrid"],
Cell[CellGroupData[{
Cell[974, 40, 692, 22, 96, "Usage",
 CellID->17713],
Cell[CellGroupData[{
Cell[1691, 66, 737, 24, 40, "NotesSection",
 CellGroupingRules->{"SectionGrouping", 50},
 CellID->75457274],
Cell[2431, 92, 31, 0, 14, "SectionHeaderSpacer"],
Cell[2465, 94, 212, 7, 36, "Notes",
 CellID->718278630],
Cell[2680, 103, 373, 14, 39, "Notes",
 CellID->136833138],
Cell[3056, 119, 366, 13, 39, "Notes",
 CellID->994190625],
Cell[3425, 134, 31, 0, 29, "SectionFooterSpacer"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[3505, 140, 315, 13, 39, "PrimaryExamplesSection",
 CellTags->"PrimaryExamplesSection",
 CellID->1195052388],
Cell[CellGroupData[{
Cell[3845, 157, 107, 2, 33, "ExampleSection",
 CellID->1978418372],
Cell[3955, 161, 162, 4, 33, "Input",
 CellID->2007368699],
Cell[4120, 167, 251, 9, 43, "ExampleText",
 CellID->477905686],
Cell[4374, 178, 197, 7, 33, "Input",
 CellID->340848625],
Cell[4574, 187, 198, 7, 33, "Input",
 CellID->1835678537],
Cell[4775, 196, 234, 8, 33, "Input",
 CellID->1888323599],
Cell[5012, 206, 125, 5, 41, "ExampleText",
 CellID->1200443100],
Cell[CellGroupData[{
Cell[5162, 215, 198, 5, 33, "Input",
 CellID->832821921],
Cell[5363, 222, 74, 2, 42, "Output",
 CellID->2097904239]
}, Open  ]],
Cell[5452, 227, 117, 5, 41, "ExampleText",
 CellID->852042784],
Cell[CellGroupData[{
Cell[5594, 236, 372, 10, 33, "Input",
 CellID->692020249],
Cell[5969, 248, 138, 4, 42, "Output",
 CellID->1260285186]
}, Open  ]],
Cell[6122, 255, 106, 3, 33, "Input",
 CellID->1247145880],
Cell[6231, 260, 151, 4, 33, "Input",
 CellID->173805481]
}, Open  ]],
Cell[6397, 267, 52, 1, 29, "SectionFooterSpacer",
 CellID->2115137253]
}, Open  ]],
Cell[CellGroupData[{
Cell[6486, 273, 265, 12, 39, "SeeAlsoSection",
 CellID->20994],
Cell[6754, 287, 1140, 34, 67, "SeeAlso",
 CellID->18286]
}, Open  ]],
Cell[CellGroupData[{
Cell[7931, 326, 278, 12, 39, "MoreAboutSection",
 CellID->1973046713],
Cell[8212, 340, 31, 0, 14, "SectionHeaderSpacer"],
Cell[8246, 342, 181, 5, 26, "MoreAbout",
 CellID->1788271703]
}, Open  ]],
Cell[8442, 350, 23, 0, 47, "FooterCell"]
}
]
*)


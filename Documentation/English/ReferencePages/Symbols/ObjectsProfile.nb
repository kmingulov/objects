(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 10.4' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     15396,        545]
NotebookOptionsPosition[     12306,        442]
NotebookOutlinePosition[     13421,        477]
CellTagsIndexPosition[     13336,        472]
WindowTitle->ObjectsProfile
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[GridBox[{
   {Cell["OBJECTS PACKAGE SYMBOL", "PacletNameCell"]}
  }]], "AnchorBarGrid",
 CellID->1],

Cell[TextData[{
 Cell["ObjectsProfile", "ObjectName"],
 Cell[BoxData[
  InterpretationBox[
   StyleBox[
    GraphicsBox[{},
     BaselinePosition->Baseline,
     ImageSize->{8, 0}],
    CacheGraphics->False],
   Spacer[8]]]]
}], "ObjectNameGrid"],

Cell[CellGroupData[{

Cell[BoxData[GridBox[{
   {"", Cell[TextData[{
     Cell[BoxData[
      RowBox[{
       TemplateBox[{Cell[
          TextData["ObjectsProfile"]],"paclet:Objects/ref/ObjectsProfile"},
        "RefLink",
        BaseStyle->"InlineFormula"], "[", 
       StyleBox["expr", "TI"], "]"}]], "InlineFormula"],
     " \[LineSeparator]evaluates ",
     StyleBox["expr", "TI"],
     " gathering information about time spent for evaluation of each method \
encountered in ",
     StyleBox["expr", "TI"],
     "."
    }]]}
  }]], "Usage",
 GridBoxOptions->{
 GridBoxBackground->{
  "Columns" -> {{None}}, "ColumnsIndexed" -> {}, 
   "Rows" -> {None, None, {None}}, "RowsIndexed" -> {}}},
 CellID->17713],

Cell[CellGroupData[{

Cell[TextData[Cell[BoxData[
 ButtonBox[Cell[TextData[{
   Cell[BoxData[
    InterpretationBox[
     StyleBox[
      GraphicsBox[{},
       BaselinePosition->Baseline,
       ImageSize->{6, 0}],
      CacheGraphics->False],
     Spacer[6]]]],
   "Details"
  }], "NotesFrameText"],
  Appearance->{Automatic, None},
  BaseStyle->None,
  ButtonFunction:>(FrontEndExecute[{
     FrontEnd`SelectionMove[
      FrontEnd`SelectedNotebook[], All, ButtonCell], 
     FrontEndToken["OpenCloseGroup"], 
     FrontEnd`SelectionMove[
      FrontEnd`SelectedNotebook[], After, CellContents]}]& ),
  Evaluator->None,
  Method->"Preemptive"]]]], "NotesSection",
 WholeCellGroupOpener->True,
 CellGroupingRules->{"SectionGrouping", 50},
 CellID->75457274],

Cell["", "SectionHeaderSpacer"],

Cell[TextData[{
 "To use this function, you first need to load the ",
 ButtonBox["Objects Package",
  BaseStyle->"Link",
  ButtonData->"paclet:Objects/guide/ObjectsPackage"],
 "."
}], "Notes",
 CellID->718278630],

Cell[TextData[{
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["ObjectsProfile"]],"paclet:Objects/ref/ObjectsProfile"},
   "RefLink",
   BaseStyle->"InlineFormula"]], "InlineFormula"],
 " returns a list of form ",
 Cell[BoxData[
  RowBox[{"{", 
   RowBox[{
    StyleBox["res", "TI"], ",", 
    StyleBox["pdata", "TI"]}], "}"}]], "InlineFormula"],
 ", where ",
 Cell[BoxData[
  StyleBox["res", "TI"]], "InlineFormula"],
 " is the result of computation and ",
 Cell[BoxData[
  StyleBox["pdata", "TI"]], "InlineFormula"],
 " is profiler data."
}], "Notes",
 CellID->136833138],

Cell[TextData[{
 "Profiler date ",
 Cell[BoxData[
  StyleBox["pdata", "TI"]], "InlineFormula"],
 " is an ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["Association"]],"ref/Association"},
   "RefLink",
   BaseStyle->"InlineFormula"]], "InlineFormula"],
 ". Its keys have form ",
 Cell[BoxData[
  RowBox[{"{", 
   RowBox[{
    StyleBox["obj", "TI"], ",", 
    StyleBox["method", "TI"]}], "}"}]], "InlineFormula"],
 " and its values have form ",
 Cell[BoxData[
  RowBox[{"{", 
   RowBox[{
    StyleBox["total", "TI"], ",", 
    StyleBox["n", "TI"], ",", 
    RowBox[{"{", 
     RowBox[{
      SubscriptBox[
       StyleBox["t", "TI"], "1"], ",", 
      SubscriptBox[
       StyleBox["t", "TI"], "2"], ",", "\[Ellipsis]"}], "}"}]}], "}"}]], 
  "InlineFormula"],
 ". Here ",
 Cell[BoxData[
  StyleBox["total", "TI"]], "InlineFormula"],
 " is total time spent in this method, ",
 Cell[BoxData[
  StyleBox["n", "TI"]], "InlineFormula"],
 " is a number of runs, and ",
 Cell[BoxData[
  SubscriptBox[
   StyleBox["t", "TI"], "i"]], "InlineFormula"],
 " is time spent in ",
 Cell[BoxData["i"], "InlineFormula"],
 "-th run."
}], "Notes",
 CellID->1421328722],

Cell["", "SectionFooterSpacer"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[{
 Cell[BoxData[
  InterpretationBox[
   StyleBox[
    GraphicsBox[{},
     BaselinePosition->Baseline,
     ImageSize->{6, 0}],
    CacheGraphics->False],
   Spacer[6]]]],
 "Examples"
}], "PrimaryExamplesSection",
 WholeCellGroupOpener->True,
 CellTags->"PrimaryExamplesSection",
 CellID->1195052388],

Cell[CellGroupData[{

Cell["Basic example", "ExampleSection", "ExampleSection",
 WholeCellGroupOpener->True,
 CellID->1978418372],

Cell[BoxData[
 RowBox[{"<<", "Objects`"}]], "Input",
 CellChangeTimes->{{3.703571888770885*^9, 3.703571890101639*^9}},
 CellLabel->"In[1]:=",
 CellID->2007368699],

Cell[TextData[{
 "Create several classes with ",
 Cell[BoxData[
  TemplateBox[{Cell[
     TextData["CNew"]],"paclet:Objects/ref/CNew"},
   "RefLink",
   BaseStyle->"InlineFormula"]], "InlineFormula"],
 ", that have a long-running method ",
 Cell[BoxData["wait"], "InlineFormula"],
 ":"
}], "ExampleText",
 CellID->477905686],

Cell[BoxData[
 RowBox[{
  RowBox[{"CNew", "[", 
   RowBox[{"ClassA", ",", "\[IndentingNewLine]", 
    RowBox[{"{", "}"}], ",", "\[IndentingNewLine]", 
    RowBox[{"{", "\[IndentingNewLine]", 
     RowBox[{"$Method", "[", 
      RowBox[{"wait", ",", 
       RowBox[{"{", "this", "}"}], ",", "\[IndentingNewLine]", 
       RowBox[{
        RowBox[{"Pause", "[", "0.5", "]"}], ";", "\[IndentingNewLine]", 
        "1"}]}], "\[IndentingNewLine]", "]"}], "\[IndentingNewLine]", "}"}]}],
    "]"}], ";"}]], "Input",
 CellLabel->"In[2]:=",
 CellID->1405712866],

Cell[BoxData[
 RowBox[{
  RowBox[{"CNew", "[", 
   RowBox[{"ClassB", ",", "\[IndentingNewLine]", 
    RowBox[{"{", "child", "}"}], ",", "\[IndentingNewLine]", 
    RowBox[{"{", "\[IndentingNewLine]", 
     RowBox[{"$Method", "[", 
      RowBox[{"wait", ",", 
       RowBox[{"{", "this", "}"}], ",", "\[IndentingNewLine]", 
       RowBox[{
        RowBox[{"(", 
         RowBox[{
          RowBox[{"Pause", "[", "0.3", "]"}], ";", "1"}], ")"}], "+", 
        RowBox[{
         RowBox[{"this", "[", "child", "]"}], "[", "wait", "]"}], "+", 
        RowBox[{"(", 
         RowBox[{
          RowBox[{"Pause", "[", "0.2", "]"}], ";", "2"}], ")"}]}]}], 
      "\[IndentingNewLine]", "]"}], "\[IndentingNewLine]", "}"}], ",", 
    "\[IndentingNewLine]", 
    RowBox[{"$Constructor", "[", 
     RowBox[{
      RowBox[{"{", 
       RowBox[{"this", ",", "$child"}], "}"}], ",", "\[IndentingNewLine]", 
      RowBox[{
       RowBox[{
        RowBox[{"this", "[", "child", "]"}], "=", "$child"}], ";"}]}], 
     "\[IndentingNewLine]", "]"}]}], "\[IndentingNewLine]", "]"}], 
  ";"}]], "Input",
 CellLabel->"In[3]:=",
 CellID->1744824825],

Cell[BoxData[
 RowBox[{
  RowBox[{"CNew", "[", 
   RowBox[{"ClassC", ",", "\[IndentingNewLine]", 
    RowBox[{"{", "child", "}"}], ",", "\[IndentingNewLine]", 
    RowBox[{"{", "\[IndentingNewLine]", 
     RowBox[{"$Method", "[", 
      RowBox[{"wait", ",", 
       RowBox[{"{", "this", "}"}], ",", "\[IndentingNewLine]", 
       RowBox[{
        RowBox[{"(", 
         RowBox[{
          RowBox[{"Pause", "[", "0.1", "]"}], ";", "1"}], ")"}], "+", 
        RowBox[{
         RowBox[{"this", "[", "child", "]"}], "[", "wait", "]"}], "+", 
        RowBox[{"(", 
         RowBox[{
          RowBox[{"Pause", "[", "0.2", "]"}], ";", "2"}], ")"}], "+", 
        RowBox[{
         RowBox[{"this", "[", "child", "]"}], "[", "wait", "]"}], "+", 
        RowBox[{"(", 
         RowBox[{
          RowBox[{"Pause", "[", "0.2", "]"}], ";", "3"}], ")"}], "+", 
        RowBox[{
         RowBox[{"this", "[", "child", "]"}], "[", "wait", "]"}], "+", 
        RowBox[{"(", 
         RowBox[{
          RowBox[{"Pause", "[", "0.1", "]"}], ";", "4"}], ")"}]}]}], 
      "\[IndentingNewLine]", "]"}], "\[IndentingNewLine]", "}"}], ",", 
    "\[IndentingNewLine]", 
    RowBox[{"$Constructor", "[", 
     RowBox[{
      RowBox[{"{", 
       RowBox[{"this", ",", "$child"}], "}"}], ",", "\[IndentingNewLine]", 
      RowBox[{
       RowBox[{
        RowBox[{"this", "[", "child", "]"}], "=", "$child"}], ";"}]}], 
     "\[IndentingNewLine]", "]"}]}], "\[IndentingNewLine]", "]"}], 
  ";"}]], "Input",
 CellLabel->"In[4]:=",
 CellID->1124352282],

Cell["Create instances of these classes:", "ExampleText",
 CellID->427719378],

Cell[BoxData[{
 RowBox[{
  RowBox[{"ONew", "[", 
   RowBox[{"objA", ",", "ClassA"}], "]"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"ONew", "[", 
   RowBox[{"objB", ",", "ClassB", ",", "objA"}], "]"}], ";"}], "\n", 
 RowBox[{
  RowBox[{"ONew", "[", 
   RowBox[{"objC", ",", "ClassC", ",", "objB"}], "]"}], ";"}]}], "Input",
 CellChangeTimes->{{3.706860400977618*^9, 3.706860407948401*^9}, {
   3.706860942892624*^9, 3.7068609486830587`*^9}, 3.706861168260363*^9},
 CellLabel->"In[5]:=",
 CellID->1491303143],

Cell["Run profiler:", "ExampleText",
 CellID->867217232],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"ObjectsProfile", "[", 
  RowBox[{"objC", "[", "wait", "]"}], "]"}]], "Input",
 CellChangeTimes->{{3.7068604352065897`*^9, 3.7068604525888577`*^9}, {
   3.70686095410345*^9, 3.7068609572370453`*^9}, 3.706861002966872*^9},
 CellLabel->"In[8]:=",
 CellID->2144593648],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"22", ",", 
   RowBox[{"\[LeftAssociation]", 
    RowBox[{
     RowBox[{
      RowBox[{"{", 
       RowBox[{"objA", ",", "wait"}], "}"}], "\[Rule]", 
      RowBox[{"{", 
       RowBox[{"1.503448`6.151512150458871", ",", "3", ",", 
        RowBox[{"{", 
         RowBox[{
         "0.50141`6.151737984668852", ",", "0.501455`6.15177695950933", ",", 
          "0.500583`6.1510210902033435"}], "}"}]}], "}"}]}], ",", 
     RowBox[{
      RowBox[{"{", 
       RowBox[{"objB", ",", "wait"}], "}"}], "\[Rule]", 
      RowBox[{"{", 
       RowBox[{"1.506431`5.851342987315848", ",", "3", ",", 
        RowBox[{"{", 
         RowBox[{
         "0.502199`5.851390841424119", ",", "0.50273`5.851849800000004", ",", 
          "0.501502`5.8507876671669585"}], "}"}]}], "}"}]}], ",", 
     RowBox[{
      RowBox[{"{", 
       RowBox[{"objC", ",", "wait"}], "}"}], "\[Rule]", 
      RowBox[{"{", 
       RowBox[{"0.603561`5.630206171588324", ",", "1", ",", 
        RowBox[{"{", "0.603561`5.630206171588324", "}"}]}], "}"}]}]}], 
    "\[RightAssociation]"}]}], "}"}]], "Output",
 CellLabel->"Out[8]=",
 CellID->175336636]
}, Open  ]],

Cell[BoxData[
 RowBox[{
  RowBox[{"ODelete", "[", 
   RowBox[{"objC", ",", "objB", ",", "objA"}], "]"}], ";"}]], "Input",
 CellLabel->"In[9]:=",
 CellID->1065716003],

Cell[BoxData[
 RowBox[{
  RowBox[{"CDelete", "[", 
   RowBox[{"ClassA", ",", "ClassB", ",", "ClassC"}], "]"}], ";"}]], "Input",
 CellLabel->"In[10]:=",
 CellID->1511888532]
}, Open  ]],

Cell["", "SectionFooterSpacer",
 CellID->2115137253]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[{
 Cell[BoxData[
  InterpretationBox[
   StyleBox[
    GraphicsBox[{},
     BaselinePosition->Baseline,
     ImageSize->{6, 0}],
    CacheGraphics->False],
   Spacer[6]]]],
 "See Also"
}], "SeeAlsoSection",
 WholeCellGroupOpener->True,
 CellID->20994],

Cell[TextData[Cell[BoxData[
 TemplateBox[{Cell[
    TextData["ObjectsTrace"]],"paclet:Objects/ref/ObjectsTrace"},
  "RefLink",
  BaseStyle->{
   "InlineFormula", FontFamily -> "Verdana"}]], "InlineFormula"]], "SeeAlso",
 CellID->18286]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[{
 Cell[BoxData[
  InterpretationBox[
   StyleBox[
    GraphicsBox[{},
     BaselinePosition->Baseline,
     ImageSize->{6, 0}],
    CacheGraphics->False],
   Spacer[6]]]],
 "Related Guides"
}], "MoreAboutSection",
 WholeCellGroupOpener->True,
 CellID->1973046713],

Cell["", "SectionHeaderSpacer"],

Cell[BoxData[
 TemplateBox[{Cell[
    TextData["Objects Package"]],"paclet:Objects/guide/ObjectsPackage"},
  "RefLink",
  BaseStyle->"MoreAbout"]], "MoreAbout",
 CellID->1788271703]
}, Open  ]],

Cell[" ", "FooterCell"]
},
Saveable->False,
ScreenStyleEnvironment->"Working",
WindowSize->{1918, 1005},
WindowMargins->{{-9, Automatic}, {Automatic, -8}},
WindowTitle->"ObjectsProfile",
TaggingRules->{
 "ModificationHighlight" -> False, 
  "Metadata" -> {
   "context" -> "Objects`", "keywords" -> {}, "index" -> True, "label" -> 
    "Objects Package Symbol", "language" -> "en", "paclet" -> 
    "Objects Package", "status" -> "", "summary" -> "", "synonyms" -> {}, 
    "title" -> "ObjectsProfile", "type" -> "Symbol", "uri" -> 
    "Objects/ref/ObjectsProfile"}, "SearchTextTranslated" -> "", "LinkTrails" -> 
  ""},
FrontEndVersion->"10.4 for Linux x86 (64-bit) (April 11, 2016)",
StyleDefinitions->FrontEnd`FileName[{"Wolfram"}, "Reference.nb", 
  CharacterEncoding -> "UTF-8"]
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{
 "PrimaryExamplesSection"->{
  Cell[4492, 175, 315, 13, 39, "PrimaryExamplesSection",
   CellTags->"PrimaryExamplesSection",
   CellID->1195052388]}
 }
*)
(*CellTagsIndex
CellTagsIndex->{
 {"PrimaryExamplesSection", 13192, 465}
 }
*)
(*NotebookFileOutline
Notebook[{
Cell[586, 21, 112, 3, 43, "AnchorBarGrid",
 CellID->1],
Cell[701, 26, 246, 10, 61, "ObjectNameGrid"],
Cell[CellGroupData[{
Cell[972, 40, 690, 21, 97, "Usage",
 CellID->17713],
Cell[CellGroupData[{
Cell[1687, 65, 737, 24, 40, "NotesSection",
 CellGroupingRules->{"SectionGrouping", 50},
 CellID->75457274],
Cell[2427, 91, 31, 0, 14, "SectionHeaderSpacer"],
Cell[2461, 93, 212, 7, 36, "Notes",
 CellID->718278630],
Cell[2676, 102, 576, 20, 40, "Notes",
 CellID->136833138],
Cell[3255, 124, 1154, 43, 40, "Notes",
 CellID->1421328722],
Cell[4412, 169, 31, 0, 29, "SectionFooterSpacer"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[4492, 175, 315, 13, 39, "PrimaryExamplesSection",
 CellTags->"PrimaryExamplesSection",
 CellID->1195052388],
Cell[CellGroupData[{
Cell[4832, 192, 107, 2, 33, "ExampleSection",
 CellID->1978418372],
Cell[4942, 196, 162, 4, 33, "Input",
 CellID->2007368699],
Cell[5107, 202, 324, 11, 43, "ExampleText",
 CellID->477905686],
Cell[5434, 215, 553, 14, 195, "Input",
 CellID->1405712866],
Cell[5990, 231, 1126, 30, 264, "Input",
 CellID->1744824825],
Cell[7119, 263, 1526, 40, 264, "Input",
 CellID->1124352282],
Cell[8648, 305, 77, 1, 40, "ExampleText",
 CellID->427719378],
Cell[8728, 308, 518, 13, 80, "Input",
 CellID->1491303143],
Cell[9249, 323, 56, 1, 40, "ExampleText",
 CellID->867217232],
Cell[CellGroupData[{
Cell[9330, 328, 288, 6, 33, "Input",
 CellID->2144593648],
Cell[9621, 336, 1148, 31, 66, "Output",
 CellID->175336636]
}, Open  ]],
Cell[10784, 370, 165, 5, 33, "Input",
 CellID->1065716003],
Cell[10952, 377, 172, 5, 33, "Input",
 CellID->1511888532]
}, Open  ]],
Cell[11139, 385, 52, 1, 29, "SectionFooterSpacer",
 CellID->2115137253]
}, Open  ]],
Cell[CellGroupData[{
Cell[11228, 391, 265, 12, 39, "SeeAlsoSection",
 CellID->20994],
Cell[11496, 405, 235, 6, 67, "SeeAlso",
 CellID->18286]
}, Open  ]],
Cell[CellGroupData[{
Cell[11768, 416, 278, 12, 39, "MoreAboutSection",
 CellID->1973046713],
Cell[12049, 430, 31, 0, 14, "SectionHeaderSpacer"],
Cell[12083, 432, 181, 5, 26, "MoreAbout",
 CellID->1788271703]
}, Open  ]],
Cell[12279, 440, 23, 0, 47, "FooterCell"]
}
]
*)


(* ::Package:: *)

(* OBJECTS INSTALLATION SCRIPT *)

Module[{objects, oldHeaders, errors = False, json, file, path},

	(* Formatted name. *)
	objects="\!\(\*StyleBox[\"Objects\",FontWeight->\"Bold\",FontColor->RGBColor[1,0.5,0]]\)";
	Print["Preparing to install ", objects, "."];

	(* Checking for ProjectInstaller. *)
	If[Quiet[Needs["ProjectInstaller`"]] === $Failed,
		(* ===== PI NOT FOUND ===== *)
		Print["ProjectInstaller by L. Shifrin was not found. Installing it."];
		Get["https://raw.github.com/lshifr/ProjectInstaller/master/BootstrapInstall.m"];
		Needs["ProjectInstaller`"];,

		(* ===== PI FOUND ===== *)
		Print["Using ProjectInstaller by L. Shifrin."];
	];

	(* Modifying HTTP headers, used by URLTools functions. *)
	(* This fix was taken from BootstrapInstaller by jkuczm, licensed under MIT: *)
	(*   https://github.com/jkuczm/MathematicaBootstrapInstaller *)
	oldHeaders = OptionValue[Utilities`URLTools`Private`FetchURLInternal,
		"RequestHeaderFields"
	];
	SetOptions[Utilities`URLTools`Private`FetchURLInternal,
		"RequestHeaderFields" -> {"Content-Type" -> ""}
	];

	(* Installing the package. *)
	CheckAbort[
		(* Getting the latest version. *)
		Check[
			json = Import["https://api.bitbucket.org/2.0/repositories/kmingulov/objects/downloads","JSON"],
			Print["Failed to connect to the remote repository."];
			Abort[];
		];
		json = "values" /. json;
		If[Length@json == 0,
			Print["The remote repository is empty, cannot download ", objects, "."];
			Abort[];
		];
		file = "name" /. json[[1]];
		file = "https://bitbucket.org/kmingulov/objects/downloads/" <> file;

		(* Installing. *)
		Print["Installing ", file, "\[Ellipsis]"];
		Quiet[path = ProjectInstaller`ProjectInstall[URL[file]]];
		If[path === $Failed,
			Print["Uninstalling the previous version\[Ellipsis]"];
			ProjectInstaller`ProjectUninstall["Objects"];
			path = ProjectInstaller`ProjectInstall[URL[file]];
		];

		(* Done. *)
		Print["Installation of ", objects, " is complete. You can load the package by <<Objects`."];
		Print["Package is located at: ", path, "."];,

		(* Error occured. *)
		errors = True;
	];

	(* Setting headers back. *)
	SetOptions[Utilities`URLTools`Private`FetchURLInternal,
		"RequestHeaderFields" -> oldHeaders
	];

	(* Was there an error? Printing information. *)
	If[errors,
		Print["\!\(\*StyleBox[\"FAILED!\",FontWeight->\"Bold\",FontColor->RGBColor[1,0,0]]\) Please, try to install ", objects, " manually."]
	];
]

Objects
=======
**Objects** is a *Wolfram Mathematica* package, which introduces simple classes and objects, allowing to write object-oriented code.

**Objects** is a free software and is distributed under the terms of GNU GPL v3.

*Official repository:* [bitbucket.org/kmingulov/objects/](https://bitbucket.org/kmingulov/objects/)

*Version:* 1.31 (2 November 2017)

*Author:* Kirill T. Mingulov

Features
--------

* Classes and objects.
* Classes inheritance, abstract methods.
* Methods reimplementation.
* Functions for checks and queries on objects and classes.
* Functions for profiling and debugging.

Installation
------------
There are two ways to install **Objects**.

### 1. Automatic online installation
You can install **Objects** using our installation script. Type in *Mathematica* session the following command:

    Import["https://bitbucket.org/kmingulov/objects/raw/master/Install.m"];

It will download the latest release of **Objects** and install it. After that, you can load the package by the following command:

	<<Objects`

### 2. Manual installation
Download the archive with the latest version of the package from the [downloads page](https://bitbucket.org/kmingulov/objects/downloads) of our repository. Follow instructions in *INSTALLATION NOTES.txt* file from the archive.

Documentation
-------------
The **Objects** package has Mathematica-like built-in documentation, which can be accessed via Documentation Center.

Bugs and feature requests
-------------------------
If you experience any problems with the package or have encountered a bug or just want something to be added into the package, please, leave an issue on our [issue tracker](https://bitbucket.org/kmingulov/objects/issues?status=new&status=open).
